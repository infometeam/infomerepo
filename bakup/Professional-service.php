<?php
include('header.php');
?>
<div class="innerarea" id="service">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"> <img src="img/serviceimg4.jpg" class="img-responsive serviceimg" alt=""/> </div>
      <div class="col-sm-8">
        <h3>Professional Service </h3>
        <p>The challenges faced by different businesses are not always the same. As such, companies require a specific, customized solution that will help them to effectively overcome the issues affecting performance. This is where Supernet comes into play. Supernet helps your business to meet business objectives by offering quality IT services &amp; support, exactly as per your requirement.</p>
        <p>Whatever be the nature of your business, we have the experience &amp; technological know how to help you find solutions to persisting IT related issues. What makes us different from other players is the fact that we align our services &amp; expertise, with the clients work culture, delivering results that guarantees satisfaction. </p>
        <p>We work closely with your IT team, complimenting their efforts by offering superior equipment &amp; expertise &amp; guide them on managing the services efficiently. By offering highly flexible &amp; scalable solution, the benefits for our clients are manifold, helping them leverage business opportunities like never before.</p>
        <p>Supernet aims to power your business with underlying superior & reliable IT support services that will take your business to the next level. Through sustained efforts, we ensure that our client stays on top, no matter what it takes!</p>
        <ul>
          <li>IT TOTAL ASSIST</li>
          <li>ONLINE OFFICE(OFFICE 365)</li>
          <li>SERVER MIGRATION</li>
          <li>SWITCHING & ROUTING</li>
          <li>PHONE SYSTEM MIGRATION</li>
          <li>NETWORK PROTECTION</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>