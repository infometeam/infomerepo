<?php
include('header.php');
?>
<div class="innerarea smartoffice" id="service">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"> <img src="img/serviceimg2.jpg" class="img-responsive serviceimg" alt=""/> </div>
      <div class="col-sm-8">
        <h3>Smart Office</h3>
        <p>Businesses around the world are transforming like never before. And the role of technology in this revolution is something we need to appreciate. Thanks to the advancement of technology in the past decades, work places have evolved into smarter & efficient ones, helping management & employees alike, saving them a lot of time & energy.</p>
        <p>Supernet understands the significance of technology in modern day business environment &strives to develop innovative solutions for our clients worldwide. Whether you are a start-up or a well-established business, we have the right solutions for all, scaled to your requirement. </p>
        <p>We believe innovation & perseverance is the key to success in this highly competitive environment & have always made continued efforts to improve in every of service. This is what helped us deliver stunning results over the years for clients from various industries. We value our customers more than anything & by emphasizing on quality of our service, we have always made sure that customer satisfaction is our top priority.</p>
        <p>Our solutions will ensure that your business communication & automation requirements are taken care of well, empowering your employees and customers alike & offering them a better experience.</p>
        <p>Join us to establish a strong foundation for your future business growth.</p>
        <ul class="clearfix">
          <li>SERVER  SOLUTION</li>
          <li>VOIP COMMUNICATION </li>
          <li>STRUCTURED CABLING</li>
          <li>DATA BACKUP </li>
          <li>VPN & FIREWALL SECURITY </li>
          <li>WIRELESS COMMUNICATION </li>
          <li>VIDEO CONFERENCE </li>
          <li>TIME ATTENDANCE & ACCESS CONTROL</li>
          <li>CCTV SURVEILLANCE (SAMSUNG, HIK VISION)</li>
          <li>CARD PRINTERS AND CARD PERSONALISATION </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>