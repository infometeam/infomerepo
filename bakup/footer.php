<div id="footermain">
  <div class="container">
    <div class="row">
      <div class="col-sm-4"><a href="index.php"><img src="img/footerlogo.jpg"  alt=""/></a></div>
      <div class="col-sm-3">
        <div class="footeraddress"> PB No: 43976, <br/>
          Dubai , U.A.E </div>
      </div>
      <div class="col-sm-3">
        <div class="footeraddress"><!-- E-Mail:<a href="mailto:email@mail.com"> email@mail.com</a> <br/>--> 
          Phone : +97143519739<br/>
          Fax : +97143519759 </div>
      </div>
      <div class="col-sm-2">
        <ul class="list-inline social">
          <li><a href="#" class="fb"></a></li>
          <li><a href="#" class="gplus"></a></li>
          <li><a href="#" class="twt"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p> Copyright � 2015 Supernet. All Rights Reserved. </p>
        <div class="powered">Powered by<a href="http://www.bodhiinfo.com/" target="_blank" class="poweredlogo"></a></div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/owl.carousel.min.js"></script> 

<script>
	$(document).ready(function(){
		$('.product_box .zoom_box').click(function(){
			cur = $(this).parents('.product_box');
			var proHead = cur.find('h3').text();
			var proImg = cur.find('img').attr('src');
			var description = cur.data('description');
			//console.log(proHead+', '+proImg+', '+description);
			$('#myModal .modal-title').text(proHead);
			$('#myModal .pro_pop_img').attr('src', proImg);
			if(description != ""){
				$('#myModal .description_wrap').html(description);
			} else{
				$('#myModal .description_wrap').html("");
			}
		});
		
		// Enquiry
		$('.enquiry_trigger').click(function(){
			var curEnqTrigger = $(this);
			var curEnqID = curEnqTrigger.parents('.product_box').data('product_id');
			$('.enquiry_frm #productID').val(curEnqID);
		});
		
		
	});
</script>


<script>
		$('#brands').owlCarousel({
			loop:true,
			margin:10,
			responsiveClass:true,
			nav:false,
			autoplay:true,
			responsive:{
				0:{
					items:1
				},
				370:{
					items:2
				},
				600:{
					items:3
				},
				1000:{
					items:4,
					loop:true
				}
			}
		})
		
		var mq = window.matchMedia( "(max-width: 480px)" );
	if (mq.matches) {
		$('.enquirybtn').click(function(){
		window.location.href = "http://supernetme.com/contact.php#contactform";
		});
	}
	else {
		$('.enquirybtn').click(function(){
			$(this).parent('#enquirymain').toggleClass('open');
		});
	}
</script>
<script>
	$(document).ready(function(){
		$('a.product_nav').click(function(e){
			e.preventDefault();
		});
	});
</script>


</body>
</html>