-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 23, 2016 at 06:32 AM
-- Server version: 5.5.48-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `supernet_superdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `productCategoryID` int(11) NOT NULL DEFAULT '0',
  `productName` varchar(50) NOT NULL DEFAULT '0',
  `thumbnailPath` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(1000) DEFAULT NULL,
  `hmFeat` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `productCategoryID`, `productName`, `thumbnailPath`, `description`, `hmFeat`) VALUES
(47, 9, 'AVAYA IP PHONE 1600', 'images/uploaded/20151231214311_0.JPG', '<h3>Two way speaker phones</h3>\r\n<p>&nbsp;</p>\r\n<p>The avaya 1600 are great invention into the world which are value priced and designed to meet todays communication needs in a low cost package&nbsp;</p>', 0),
(50, 8, 'DATAMAX E4205', 'images/uploaded/20160320095725_0.php', '<ul>\r\n<li>\r\n<h4>Direct thermal, thermal transfer(optional)</h4>\r\n</li>\r\n<li>\r\n<h4>Available in four distinct models: Basic,Advanced, Professional+</h4>\r\n</li>\r\n<li>\r\n<h4>4.25''''(108mm)@203dpi(8dots/mm)</h4>\r\n</li>\r\n<li>\r\n<h4>203 dpi[8 dots/mm],300 dpi[12 dots/mm] optional resolution.</h4>\r\n</li>\r\n</ul>', 0),
(51, 4, 'HP 280 G1 MICROTOWER PC', 'images/uploaded/20160101123847_0.jpg', '<ul>\r\n<li>Intel&reg; Core&trade; i3-4150 with Intel HD Graphics 4400 (3.5 GHz, 3 MB cache, 2 cores)&nbsp;</li>\r\n<li>4GB DDR3-1600 UDIMM.</li>\r\n<li>500GB 7200 RPM SATA Hard Disk Drive</li>\r\n<li>Intel Integrated HD Graphics or HD Graphics 4400 Varies by Processor</li>\r\n<li>Integrated Realtek ALC221 High Definition audio (all ports are stereo) Microphone and Headphone front ports (3.5mm) Line-out and Line-in rear port (3.5mm)</li>\r\n<li>Integrated 10/100/1000M Gigabit Ethernet Controller</li>\r\n<li>HP 802.11a/g/n 2x2 Wireless Dual Band PCIe (optional)&nbsp;</li>\r\n<li>Front I/O Ports Two (2) USB 2.0 ports One (1) Microphone in One (1) Headphone out</li>\r\n<li>Rear I/O Ports Four (4) USB 2.0 ports Two (2) USB 3.0 ports One (1) RJ45 network connection One (1) Audio Line out One (1) Audio Line in One (1) VGA One (1) DVI-D&nbsp;</li>\r\n</ul>', 0),
(52, 4, 'LENOVO THINKCENTRE E73', 'images/uploaded/20160101125310_0.jpg', '<ul>\r\n<li>Intel&reg; Core&trade; i3-4130 65W/ 4370/ 4350/ 4160/ 4150</li>\r\n<li>4 GB, 1600 MHz memory.</li>\r\n<li>Integrated 2013 Intel&reg; HD.</li>\r\n<li>&nbsp;PCIe x 16, Full Height (1 x 4 link) ,&nbsp;PCIe x 1, Full Height</li>\r\n<li>3.5'''' &nbsp;500GB HDD.</li>\r\n<li>(2) USB 3.0 rear (2) USB 2.0 rear (2) USB 2.0 front (1) Handset (1) Microphone (3) Audio-In / Out / MIC-In (1) RJ45 (1) VGA (1) Display Port (1) Serial Port (Optional) (1) Parallel Port (Optional).</li>\r\n<li>Integrated 10M/100M/1000M Gigabit Ethernet4.</li>\r\n</ul>', 1),
(53, 9, 'PANASONIC KX-TES 824 PBX', 'images/uploaded/20160101130535_0.jpg', '<ul>\r\n<li>\r\n<h4>Hybrid system.</h4>\r\n</li>\r\n<li>\r\n<h4>Upto 8 CO lines and 24 Extensions.</h4>\r\n</li>\r\n<li>\r\n<h4>Direct inward system access with message.</h4>\r\n</li>\r\n<li>\r\n<h4>Uniform call distribution with message.</h4>\r\n</li>\r\n<li>\r\n<h4>Caller ID display on SLT</h4>\r\n</li>\r\n</ul>', 0),
(54, 9, 'PANASONIC KX-TT7730 Phone', 'images/uploaded/20160101131031_0.jpg', '<ul>\r\n<li>\r\n<h4>Alphanumeric display.</h4>\r\n</li>\r\n<li>\r\n<h4>Programmable keys with Dual core LED.</h4>\r\n</li>\r\n<li>\r\n<h4>Feature access keys for LED.</h4>\r\n</li>\r\n<li>\r\n<h4>Hands free speech.</h4>\r\n</li>\r\n<li>\r\n<h4>Voice call.</h4>\r\n</li>\r\n</ul>', 0),
(55, 9, 'PANASONIC KX-TG3611 CORDLESS PHONE', 'images/uploaded/20160101131921_0.jpg', '<ul>\r\n<li>\r\n<h4>Caller ID compatable.</h4>\r\n</li>\r\n<li>\r\n<h4>Light Up Indicator.</h4>\r\n</li>\r\n<li>\r\n<h4>Dital handset Speaker phone</h4>\r\n</li>\r\n</ul>', 1),
(56, 9, 'AVAYA IP500 V2', 'images/uploaded/20160101132406_0.jpg', '<ul>\r\n<li>\r\n<h4>Backwards compatable with IP500.</h4>\r\n</li>\r\n<li>\r\n<h4>4 card slots compatible with existing IP500 cards as well as new Release 8.0 cards</h4>\r\n</li>\r\n<li>\r\n<h4>SD Card required, acts as Feature Key</h4>\r\n</li>\r\n<li>\r\n<h4>Operates in standard IP Office mode and Essential Edition PARTNER mode</h4>\r\n</li>\r\n</ul>', 0),
(57, 9, 'YEASTER MYPBX SOHO', 'images/uploaded/20160101132819_0.jpg', '<ul>\r\n<li>\r\n<h4>Maximum number of users : 32.</h4>\r\n</li>\r\n<li>\r\n<h4>Maximum concurrent calls : 15.</h4>\r\n</li>\r\n<li>\r\n<h4>Maximum analog ports : 4.</h4>\r\n</li>\r\n<li>\r\n<h4>LAN :1 10/100 Base-T Ethernet</h4>\r\n</li>\r\n</ul>', 0),
(58, 9, 'NEC SL1000 IP-PBX', 'images/uploaded/20160101133317_0.jpg', '<ul>\r\n<li>\r\n<h4>Room monitoring from outside.</h4>\r\n</li>\r\n<li>\r\n<h4>Warning message during night mode.</h4>\r\n</li>\r\n<li>\r\n<h4>Remote inspection with auto emergency call.</h4>\r\n</li>\r\n<li>\r\n<h4>Built in auto answer.</h4>\r\n</li>\r\n<li>\r\n<h4>TDM/IP coverage.</h4>\r\n</li>\r\n</ul>', 0),
(59, 9, 'CISCO SPA502G', 'images/uploaded/20160101134535_0.jpg', '<ul>\r\n<li>\r\n<h4>One voice line.</h4>\r\n</li>\r\n<li>\r\n<h4>Menu driven user interface.</h4>\r\n</li>\r\n<li>\r\n<h4>Shared line appearance.</h4>\r\n</li>\r\n<li>\r\n<h4>Caller ID name and number.</h4>\r\n</li>\r\n<li>\r\n<h4>Outbound caller ID blocking.</h4>\r\n</li>\r\n</ul>', 0),
(60, 9, 'AVAYA 9608 IP PHONE', 'images/uploaded/20160101135406_0.jpg', '<ul>\r\n<li>\r\n<h4>Monochrome display -3.2''''X 2.2''''.</h4>\r\n</li>\r\n<li>\r\n<h4>8 Buttons with dual LED.</h4>\r\n</li>\r\n<li>\r\n<h4>HArd buttons for phone, messages, contacts, history, name, home, navigation, cluster, headset, speaker.&nbsp;</h4>\r\n</li>\r\n<li>\r\n<h4>24 administrative buttons.</h4>\r\n</li>\r\n</ul>', 0),
(61, 9, 'NEC IP PHONE 12 TXH A', 'images/uploaded/20160101190638_0.jpg', '<ul>\r\n<li>\r\n<h4>Full duplex hands free.</h4>\r\n</li>\r\n<li>\r\n<h4>Equiped with RJ45 jacks for LAN and PC connections.</h4>\r\n</li>\r\n<li>\r\n<h4>Backlite 3 line 24 character display.</h4>\r\n</li>\r\n<li>\r\n<h4>User programmable function keys with Red/Green LEDs</h4>\r\n</li>\r\n</ul>', 0),
(62, 9, 'YEALINK SIP T38 PHONE', 'images/uploaded/20160101191929_0.jpg', '<ul>\r\n<li>TI Aries chipset and TI voice engine.</li>\r\n<li>Dual port Gigabit Ethernet&nbsp;</li>\r\n<li>Power over Ethernet.</li>\r\n<li>4.3'''' TFT -LCD ,480 X 272 pixel,16.7 M colours.</li>\r\n<li>Color picture caller-ID,Screen Saver, Wallpaper.</li>\r\n</ul>', 1),
(63, 5, 'DELL INSPIRON 5558', 'images/uploaded/20160101192913_0.JPG', '<ul>\r\n<li>Upto 6th generation Intel core processors.Web pages, games and apps fire up fast.</li>\r\n<li>With upto 1TB of storage space,you have plenty of rooms to store your files.</li>\r\n<li>Great battery life upto 7 hours.</li>\r\n<li>Long range wifi : Newest&nbsp;802.11 AC technology increses the wifi range.</li>\r\n</ul>', 0),
(64, 5, 'HP PAVILION DV7T', 'images/uploaded/20160101193533_0.JPG', '<ul>\r\n<li>Sleek, futuristic ''liquid metal'' finish.</li>\r\n<li>Display : 17.0'''' 1680 X 1050 pixels resolution.</li>\r\n<li>Primary graphics chipset : Nvidia GeForce 9600M GT.</li>\r\n<li>Battery life : 3 hours 30 minutes.</li>\r\n</ul>', 0),
(65, 5, 'LENOVO Y50 -70', 'images/uploaded/20160101194532_0.jpg', '<ul>\r\n<li>Display:15.6'''' FHD IPS LED Backlight.</li>\r\n<li>Hard drive : Hybrid 1TB 5400 RPM + 8GB SSHD.</li>\r\n<li>Graphics : Nvidia GeForce GTX 960M 2GB.</li>\r\n<li>Bluetooth version 4.0.</li>\r\n<li>Battery: 4 cells 54 Watt Hour lithium -ion.</li>\r\n</ul>', 1),
(66, 5, 'DELL INSPIRON 3542', 'images/uploaded/20160101195702_0.JPG', '<ul>\r\n<li>Upto 8GB single Channel DDR3 SDRAM at 1600MHZ - 1 DIMMS</li>\r\n</ul>', 0),
(67, 8, 'ZEBRA LABEL PRINTER GC420T', 'images/uploaded/20160101210743_0.jpg', '<ul>\r\n<li>Resolution: 203 dpi/ 8 dots per minute.</li>\r\n<li>Memory: 8Mb flash,8 Mb SDRAM.</li>\r\n<li>Print width :4.09''/104mm.</li>\r\n<li>Print length:39''''/990mm</li>\r\n<li>MAximum media roll size: 5''''/127mm O.D on a 1.00''''/25.4 mm,1.5''''/38mm ID core&nbsp;</li>\r\n</ul>', 1),
(68, 8, 'SYMBOL BARCODE SCANNER LS2208', 'images/uploaded/20160101211243_0.jpg', '<ul>\r\n<li>Dimensions: 6''''H X 2.5'''' W X 3.34'''' D.</li>\r\n<li>Weight : 5.29oz /150gm.</li>\r\n<li>Color: Cash register white or twilight black.</li>\r\n<li>Light source : 650 nm visible laser code.</li>\r\n<li>Print contrast : 20% minimum reflective differences</li>\r\n</ul>', 1),
(69, 8, 'HP RP7800 POS SYSTEM', 'images/uploaded/20160101213115_0.jpg', '<ul>\r\n<li>Processor:Inter core i3.</li>\r\n<li>Chipset: Intel Q67 Express.</li>\r\n<li>Memory: 4GB 1600MHz DDR3 SDRAM &nbsp;</li>\r\n<li>Dimensions: 36.6 x4.3 x 4 cm,17'''' display.</li>\r\n<li>Network interface: Integrated intel 82579 LM Gigabit Ethernet Network Connection.</li>\r\n</ul>', 1),
(70, 8, 'ANTI SHOPLIFTING TAGS', 'images/uploaded/20160101221319_0.jpg', '<ul>\r\n<li>58 KHz AM</li>\r\n<li>Supertag series detacher.</li>\r\n<li>All 59 Khz AM system.</li>\r\n</ul>', 0),
(71, 8, 'HP RP3000 POS SYTEM', 'images/uploaded/20160101222342_0.jpg', '<ul>\r\n<li>Chipset: Intel 94GC with ICH7.</li>\r\n<li>Memory : DDR2 Synch RAM with non ECC memory.</li>\r\n<li>Support upto 2 GB of DDR2 synch DRAM.</li>\r\n<li>Graphics: Integrated Intel graphics media accelerator 950.</li>\r\n</ul>', 1),
(72, 8, 'DATALOGIC QW2100 BARCODE SCANNER', 'images/uploaded/20160101223110_0.jpg', '<ul>\r\n<li>Wide scan angle.</li>\r\n<li>Laser-like thinner and extended scan lines.</li>\r\n<li>Drop resistence to 1.5 m/50 sq ft.</li>\r\n<li>Water and particulate sealing rating :IP42.</li>\r\n</ul>', 0),
(73, 8, 'DATALOGIC COBALTO CO5300 LASER SCANNER', 'images/uploaded/20160101223734_0.jpg', '<ul>\r\n<li>''Ring Of Light'' visual good-read confirmation.</li>\r\n<li>Configurable polyphonic speaker for customized audio good read confirmation.</li>\r\n<li>Adjustable scan head with 30 dgree tilting ability for larger items.</li>\r\n<li>Capacitive trigger for single line targeting applications such as price look up lists.</li>\r\n<li>Ergonomic rubber finishing grips are ideal for handheld operations.</li>\r\n</ul>', 1),
(74, 8, 'CASH DRAWER', 'images/uploaded/20160101224317_0.jpg', '<ul>\r\n<li>Highly reliable.</li>\r\n<li>Easy to operate.</li>\r\n<li>Highly recommended designs.</li>\r\n</ul>', 0),
(75, 8, 'RIBBONS FOR LABELS', 'images/uploaded/20160101225635_0.jpg', '<ul>\r\n<li>Ribbons coming in all dimensions.</li>\r\n</ul>', 0),
(76, 8, 'STAR SP 700 PRINTER RIBBON', 'images/uploaded/20160101225959_0.jpg', '', 0),
(77, 8, 'MAGELLAN 8400 DESK SCANNERS', 'images/uploaded/20160101230534_0.jpg', '<ul>\r\n<li>&nbsp;Aggressive and ergonomic 360 degree 5 sided scanning .</li>\r\n<li>Compatible with Mettler, Bizerba and other adaptive scales</li>\r\n<li>Optional value added features.</li>\r\n</ul>', 1),
(78, 5, 'HP 250 G4 NOTEBOOK', 'images/uploaded/20160102172406_0.JPG', '<ul>\r\n<li>Screen Size: 15.6''''.</li>\r\n<li>Dimensions: 38.43 X 24.46 X 2.43 cm.</li>\r\n<li>Weight : Starting at 2.1Kgs.</li>\r\n<li>Maximum memory : 8GB DDR3L -1600 SDRAM.</li>\r\n</ul>', 0),
(79, 5, 'TOSHIBA R30 A1040', 'images/uploaded/20160102173222_0.JPG', '<ul>\r\n<li>Screen Size: 13''''.</li>\r\n<li>Processor: 4rth Generation Intel i3,i5.</li>\r\n<li>Battery life: Upto 9 hours.</li>\r\n<li>Weights: Starting at 1.5 Kgs.</li>\r\n</ul>', 1),
(89, 5, 'ACER ASPIRE E1-572', 'images/uploaded/20160102174547_0.JPG', '<ul>\r\n<li>CPU: 1.6 GHz Intel core i5-4200U dual core processor.</li>\r\n<li>Hard drive speed: 5400 rpm.</li>\r\n<li>Native resolution: 1366 x 768.</li>\r\n<li>Graphics card: Intel HD graphics 4400.</li>\r\n<li>Touch pad size: 4.25 x 2.5''''.</li>\r\n</ul>', 0),
(91, 23, 'HP PROLIANT ML310 E', 'images/uploaded/20160102183732_0.JPG', '<ul>\r\n<li>Latest Intel Xeon E3-1200 v3 series processors.</li>\r\n<li>Dedicated iLo connection for faster and more secured data transmission.</li>\r\n<li>Windows server 2012 compatibility.</li>\r\n<li>Energy efficient Ethernet network adapter.</li>\r\n<li>Seamless network connectivity &nbsp;that extends the benefits of converged infra structure.</li>\r\n</ul>', 1),
(92, 23, 'IBM EXPRESS X3500M4', 'images/uploaded/20160102184600_0.JPG', '<ul>\r\n<li>Xeon 6C E5-2620 95W Processor.</li>\r\n<li>3 X 300GB HS 2.5IN SAS Hard drives.</li>\r\n<li>2 X 750W P/S tower</li>\r\n</ul>\r\n<p>&nbsp;</p>', 0),
(93, 23, 'HP PROLIANT DL380', 'images/uploaded/20160102185857_0.JPG', '<ul>\r\n<li>HP NVMe PCI e SSD s Support upto 1.6 TB.</li>\r\n<li>New Intel and NVIDIA GPUs.</li>\r\n<li>HP oneview for HP proliant rack Gen 9 Servers provides infra structure that reduces complexity with automation simplicity.</li>\r\n</ul>', 0),
(94, 23, 'IBM EXPRESS X3650M4', 'images/uploaded/20160102200315_0.JPG', '<ul>\r\n<li>Rack type server.</li>\r\n<li>Xeon 6C E-2620 Series processor.</li>\r\n<li>2.5'''' SAS Hard drives.</li>\r\n<li>550 W P/S RACK.</li>\r\n</ul>', 1),
(95, 7, 'HP LASERJET PRO MFP M125NW', 'images/uploaded/20160105205141_0.JPG', '<ul>\r\n<li>HP Auto On/Off technology.</li>\r\n<li>E print/ Air print &amp; WiFi direct printing.</li>\r\n<li>A4 ; A3 ; Envelops ; Post cards.</li>\r\n<li>Network and USB connectivity.</li>\r\n<li>Up to 8000 prints per month.</li>\r\n</ul>', 0),
(96, 7, 'HP LASERJET PRO MFP 225DN', 'images/uploaded/20160105214837_0.JPG', '<ul>\r\n<li>Copy resolution: Upto 600 X 600 dpi black (text and graphics),color.</li>\r\n<li>Display : 2 line LCD(text).</li>\r\n<li>Fax resolution: Upto 300 X 300 dpi (Halftone enabled).</li>\r\n<li>Paper handling : 250 sheets input tray, 100 sheet output tray.</li>\r\n<li>Print upto 8000 pages.</li>\r\n<li>Hi speed USB 2.0 connectivity.</li>\r\n</ul>', 1),
(97, 7, 'CANON MAXIFY MB 2020', 'images/uploaded/20160306221159_0.jpg', '<ul>\r\n<li>PRINTS UPTO 16 ISO IPM * IN BLACK, UPTO 11 ISO IPM IN COLOR</li>\r\n<li>4 IN 1 FUNCTIONALITY.</li>\r\n<li>BUILT IN WIRELESS LAN.</li>\r\n<li>SCAN TO USB FLASH MEMORY, EMAIL, CLOUD AND NETWORK FOLDER</li>\r\n</ul>', 1),
(98, 29, 'Y', 'images/uploaded/20160416150506_0.PNG', '<p>Y</p>', 0),
(99, 4, 'DELL OPTIPLEX 9020', 'images/uploaded/20160416183855_0.JPG', '<ul>\r\n<li>Intel&reg; 4th generation Core&trade; i3 Dual Core.</li>\r\n<li>Intel&reg; Q87 Express Chipset.</li>\r\n<li>Integrated Intel&reg; HD Graphics 4600; supports optional discrete graphics solutions from AMD and nVidia .</li>\r\n<li>Up to 4 DIMM slots (2 for USFF); Non-ECC dual-channel 1600MHz DDR3 SDRAM, up to 32GB (16GB for USFF).</li>\r\n<li>Integrated Intel&reg; I217LM Ethernet LAN 10/100/1000; supports optional PCIe 10/100/1000 network card, optional wireless 802.11n card.</li>\r\n<li>4 External USB 3.0 ports (2 front, 2 rear) and 6 External USB 2.0 ports (2 front, 4 rear, except USFF &ndash; 4 rear only) and 2 Internal USB 2.0 (MT only); 1 RJ-45; 1 Serial; 1 VGA; 2 DisplayPort; 2 PS/2 (MT/SFF only); 2 Line-in (stereo/microphone), 2 Line-out (headphone/speaker).</li>\r\n<li>Hard Disk Drives: up to 1TB .</li>\r\n</ul>', 0),
(100, 4, 'HP ELITE DESK 800 G1', 'images/uploaded/20160416185054_0.JPG', '<ul>\r\n<li>Intel&reg; 4th Generation Core&trade; i5, i7 Processors .</li>\r\n<li>1600 MHz DDR3 SDRAM; (4) DIMM slots enabling up to 32GB, dual channel memory support25 .</li>\r\n<li>Up to 500GB SATA hard drive; up to 500GB self-encrypting solid state drive; up to 160GB solid state drive .</li>\r\n<li>(2) PCI Express x1 (v2.0) (1) PCI Express x 16 (v2.0 &ndash; wired as x4) (1 )PCI Express x16 (v3.0) (1) Optional PCI (v2.3) .</li>\r\n<li>Integrated Intel&reg; HD Graphics; Discrete AMD Radeon HD 8350, 849030, NVIDIA NVS 310, 315, or NVIDIA GeForce GT630 graphics optional .</li>\r\n<li>Integrated Intel&reg; I217LM Gigabit Network Connection; optional wireless LAN card available.</li>\r\n<li>Front: (2 )USB 2.0 ports, (2) USB 3.0 ports, microphone, headphone, Rear: (4) USB 2.0 ports, (2) USB 3.0 ports, stereo audio out, line in, RJ-45 Ethernet, PS/2 mouse and keyboard, VGA, (2) DisplayPort with multi-stream4 , power connector, RS-232 serial port .</li>\r\n<li>Internal 320W Power Supply, Active PFC, up to ', 0),
(101, 6, 'HP ENVY 15-K KEYBOARD', 'images/uploaded/20160416200630_0.jpg', '', 0),
(102, 34, 'HP DL380p GEN8 E5-2630v2 KIT', 'images/uploaded/20160417070914_0.jpg', '<h1 style="margin: 0px; padding: 0px; font-size: 32px; line-height: 32px; font-family: HPSimplified, Arial;">HP DL380p Gen8 Intel Xeon E5-2630v2 (2.6GHz/6-core/15MB/80W) Processor Kit<span style="display: block; font-size: 0.7em; unicode-bidi: embed;">(715220-B21)</span></h1>', 0),
(103, 34, 'DL380 Gen9 Intel Xeon E5-2620v3 (2.4GHz/6-core/15M', 'images/uploaded/20160417071327_0.jpg', '<h1 style="margin: 0px; padding: 0px; font-size: 32px; line-height: 32px; font-family: HPSimplified, Arial;">HP DL380 Gen9 Intel Xeon E5-2620v3 (2.4GHz/6-core/15MB/85W) Processor Kit<span style="display: block; font-size: 0.7em; unicode-bidi: embed;">(719051-B21)</span></h1>', 0),
(104, 34, 'IBM EXPRESS SERVER RAM 8GB', 'images/uploaded/20160417072026_0.jpg', '<p>EXPRESS 8GB (1x8GB, 1Rx4, 1.35V) PC3L-12800 CL11 ECC DDR3 1600MHz LP RDIMM</p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE IF NOT EXISTS `productcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `productCategory` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`ID`, `productCategory`) VALUES
(4, 'desktops'),
(5, 'PROFESSIONAL LAPTOPS'),
(6, 'laptop accessories'),
(7, 'printers'),
(8, 'pos & retail products'),
(9, 'telephone & pabx'),
(12, 'ROUTERS AND ACCESS POINTS'),
(14, 'switches'),
(23, 'SERVERS'),
(24, 'ANTI VIRUS'),
(25, 'SOFTWARE PRODUCTS'),
(26, 'WORKSTATIONS'),
(27, 'ATTENDANCE AND ACCESS CONTROLS'),
(28, 'MONITORS'),
(29, 'CABINETS'),
(30, 'IT ACCESSORIES'),
(31, 'NETWORK ACCESSORIES'),
(32, 'CCTV PRODUCTS'),
(33, 'PROFESSIONAL SERVICES'),
(34, 'SERVER ACCESSORIES');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin`
--

CREATE TABLE IF NOT EXISTS `tbllogin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(200) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbllogin`
--

INSERT INTO `tbllogin` (`ID`, `userName`, `password`, `type`) VALUES
(1, 'admin', 'db773fefc8797e47c5accca10c0299b5', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
