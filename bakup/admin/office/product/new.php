<?php include("../adminHeader.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}



</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">PRODUCTS</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control"  name="type" placeholder="Product name" value="<?php echo @$_REQUEST['type'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="type"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
			$cond="1";
			if(@$_REQUEST['type'])
			{			
				$cond=$cond." and pro.productName like'%".$_POST['type']."%'";
			}
			
			?>
		<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table table_admin pagination_table view_limitter" >
                <thead>
                  <tr>
                    <th>Sl No</th>
					<th>Product</th>
					<th>Category</th>
					<th>Thumbnail</th>	
					<th>Description</th>
					<th>In Home Page</th>							
                  </tr>
                </thead>
                <tbody>
						<?php 
						$i=1;
						$select1 = mysql_query("select pro.ID, pro.productName, pro.thumbnailPath, pro.description, pro.hmFeat, pcat.productCategory from `".TABLE_PRODUCT."` pro, `".TABLE_PRODUCT_CATEGORY."` pcat where pro.productCategoryID=pcat.ID and $cond ");
						/*$select1=mysql_query("select TT.ID,TTT.type,TT.name,TT.phone from `".TABLE_PRODUCT."` TT,`".TABLE_PRODUCT_CATEGORY."` TTT where TTT.ID=TT.vehicleTypeId AND $cond order by TT.ID desc");*/
		
						$number=mysql_num_rows($select1);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="6">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
							while($row=mysql_fetch_array($select1))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
						<td><?php echo $i; $i++;?>
						  <div class="adno-dtls"> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  </div></td>
						
						<td><?php echo $row['productName']; ?></td>	
						<td><?php echo $row['productCategory']; ?></td>	
						<td><img src="../../<?php echo $row['thumbnailPath']; ?>" alt="" width="100px;"/></td>
						<td><?= $row['description']; ?></td>
						<td><?= ($row['hmFeat'] == 1) ? 'true' : 'false' ?></td>
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12 page_numbers text-center">
                <div class="btn-group pager_selector">
                </div>
            </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ADD NEW PRODUCT</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="courseName">Product Category :*</label>
                      <select name="typeID" id="typeID" class="form-control2" required >
							<option value="">Select</option>
							<?php 
								$select2 = "select * from ".TABLE_PRODUCT_CATEGORY."";
								$res2 = mysql_query($select2);
								while($row2 = mysql_fetch_array($res2))
								{
								?>
								<option value="<?= $row2['ID']; ?>"><?= $row2['productCategory']; ?></option>
								<?php
								}
							?>				
						</select>
                    </div>                   
                    <div class="form-group">
                      <label for="countType">Product Name: </label>
                      <input type="text" required="required" class="form-control2" name="name" id="name" >
                    </div>    
                    <div class="form-group">
                      <label for="name">Thumbnail Image:* (375*285)</label>
                      <input id="image1" name="productThumb" type="file" required="required" class="file-loading" accept="image/*">
                    </div>  
                    <div class="form-group">
                    	<textarea id="tinyText" name="description"></textarea>
                    </div>       
                    <div class="form-group">
                    	<input type="checkbox" name="hmFeat" value="1" />
                    	<label>Add to home page</label>
                    </div>      
					<!--<div class="form-group">
                      <label for="countType">Phone: </label>
                      <input type="text" name="phone" id="phone" class="form-control2" >	
                    </div>-->
					</div>                 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
  <script>
  tinymce.init({
    selector: '#tinyText'
  });
  </script>
<?php include("../adminFooter.php") ?>
