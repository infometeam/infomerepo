<?php
session_start();
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="admin"))
{
//header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['typeID'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$thumbnail = $_FILES['productThumb']['tmp_name'];
				$result = date("YmdHis");//$date->format('YmdHis');
				$path_info = pathinfo($_FILES['productThumb']['name']);
				$upload_file_name = $result.'_0.'.$path_info['extension'];
				if ($App->validateImages($path_info['extension'])) {
					copy($thumbnail,"../../images/uploaded/$upload_file_name");
							
				$data['productCategoryID']	=	$App->convert($_REQUEST['typeID']);
				$data['productName']	 		 =	$App->convert($_REQUEST['name']);
				$data['thumbnailPath'] = $App->convert("images/uploaded/".$upload_file_name);
				$data['description'] = $App->convert($_REQUEST['description']);
				$data['hmFeat'] = $App->convert($_REQUEST['hmFeat']);
				//$data['phone']	  		=	$App->convert($_REQUEST['phone']);
											
					
				$db->query_insert(TABLE_PRODUCT,$data);								
				$db->close();				
				$_SESSION['msg']="Product Details Added Successfully";	
				} else {
				$_SESSION['msg']="Something went wrong. Please try again!";	
				}
								
				header("location:new.php");						
			}
				
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];  		    
		if(!$_POST['typeID'])
			{	
				$_SESSION['msg']="Error, Invalid Details.";									
				header("location:edit.php?id=$editId");	
			}
			else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				if(!empty($_FILES['productThumb']['tmp_name'])){
					$selectThumb = "select * from ".TABLE_PRODUCT." where ID=".$editId;
					$resThumb = mysql_query($selectThumb);
					$rowThumb = mysql_fetch_array($resThumb);
					$currentThumbnailPath = "../../".$rowThumb['thumbnailPath'];
					unlink($currentThumbnailPath);
					$thumbnail = $_FILES['productThumb']['tmp_name'];
					$result = date("YmdHis");//$date->format('YmdHis');
					$path_info = pathinfo($_FILES['productThumb']['name']);
					$upload_file_name = $result.'_0.'.$path_info['extension'];
					if ($App->validateImages($path_info['extension'])) {
						copy($thumbnail,"../../images/uploaded/$upload_file_name");					
						$data['thumbnailPath']	  		 =	$App->convert("images/uploaded/".$upload_file_name);	
					}
					
				}		
				
				
				
				$data['productCategoryID']	 =	$App->convert($_REQUEST['typeID']);
				$data['productName']	 		  =	$App->convert($_REQUEST['name']);
				$data['description']		= 	$App->convert($_REQUEST['description']);
				$data['hmFeat'] = $App->convert($_REQUEST['hmFeat']);
					
				$db->query_update(TABLE_PRODUCT,$data," ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Product Details Updated successfully";					
				header("location:new.php");					
			}		
		
		break;		
	// DELETE SECTION
	/*case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$selectThumbDel = "select * from ".TABLE_PRODUCT." where ID=".$id;
				$resThumbDel = mysql_query($selectThumbDel);
				$rowThumbDel = mysql_fetch_array($resThumbDel);
				$currentThumbnailPathDel = "../../".$rowThumbDel['thumbnailPath'];
				unlink($currentThumbnailPathDel);				
				
				$db->query("DELETE FROM `".TABLE_PRODUCT."` WHERE ID='{$id}'");								
				$db->close(); 
				$_SESSION['msg']="Product Deleted Successfully";					
				header("location:new.php");					
		break;	*/	
		case 'delete':
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			/*$selectProModel = "select * from `".TABLE_PRODUCT_CATEGORY."` where productID = $id";
			$resProModel = mysql_query($selectProModel);
			$i = 0;
			while($rowProModel = mysql_fetch_array($resProModel))
			{
				$modelImageArray[$i++] = $rowProModel['modelImagePath'];
			}
			for($i=0; $i<count($modelImageArray); $i++)
			{
				unlink('../../'.$modelImageArray[$i]);
			}*/
			/*$db->query("DELETE FROM `".TABLE_PRODUCT_CATEGORY."` WHERE productID='{$id}'");	*/
			
			$selectThumbDel = "select * from ".TABLE_PRODUCT." where ID=".$id;
			$resThumbDel = mysql_query($selectThumbDel);
			$rowThumbDel = mysql_fetch_array($resThumbDel);
			$currentThumbnailPathDel = "../../".$rowThumbDel['thumbnailPath'];
			unlink($currentThumbnailPathDel);				
				
			$db->query("DELETE FROM `".TABLE_PRODUCT."` WHERE ID='{$id}'");								
			$db->close(); 
			$_SESSION['msg']="Product Deleted Successfully";					
			header("location:new.php");	
						
		break;
}
?>


















