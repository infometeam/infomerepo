<?php
require('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Supernet</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href="css/owl.carousel.css" rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bg">
<div id="bannermain">
  <div id="headermain">
    <div class="white-area"><img src="img/hd-white.png" alt=""></div>
    <div class="container">
      <div class="row"> 
        <!--<div class="col-md-3 logobox">
                         
                        </div>-->
        <div class="col-md-8  headerleft clearfix"> <a href="index.php" class="logo"></a>
          <div id="navigation">
            <nav class="navbar" role="navigation"> 
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!--<a class="navbar-brand img-responsive" href="#"class="logo">gdg</a>--> 
              </div>
              
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="about.php">About Us</a></li>
                  <li>
                  	<a href="#">Products</a>
                  	<ul class="sub_nav">
                  		<li>
                  			<div class="sub_nava">
                  			<ul>
                  				<?php
		                  		$navSelect = "select * from ".TABLE_PRODUCT_CATEGORY;
		                  		$navRes = mysqli_query($connection, $navSelect);
		                  		$j = mysqli_num_rows($navRes);
		                  		if($j > 0){
		                  			$i = 0;
									while($navRow = mysqli_fetch_array($navRes)){
									
									?>
									<li><a href="products.php?category=<?= $navRow['ID']; ?>"><?= $navRow['productCategory']; ?></a></li>
									<?php
									$i += 1;
									if($i == ceil($j / 2)){
									?>
									</ul></div>
									<div class="sub_navb">
									<ul>
									<?php
									}
									}
								}
		                  		?>
                  			</ul></div>
                  		</li>
                  		
                  	</ul>
                  </li>
                  <li><a href="contact.php">Contact Us</a></li>
                </ul>
              </div>
              <!-- /.navbar-collapse --> 
            </nav>
          </div>
        </div>
        <div class="col-md-4 headerright">
          <div class="headermsg"> <a href="mailto:sales@supernetme.com">sales@supernetme.com</a> </div>
          <div class="headermob"> +97143519739 </div>
        </div>
      </div>
    </div>
  </div>
  <!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
    
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="img/banner1.jpg" alt=""> </div>
      <div class="item"> <img src="img/banner2.jpg" alt=""> </div>
      <div class="item"> <img src="img/banner3.jpg" alt=""> </div>
    </div>
    
    
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>-->
</div>
<div class="container">
  <!--<div id="homeservicelinkarea" class="clearfix">
    <ul class="clearfix">
      <li class="darkbluebg"> <a href="cloud.html">
        <div class="servicewhiteicon1"></div>
        Cloud </a> </li>
      <li> <a href="Smart-office.html">
        <div class="servicewhiteicon2"></div>
        Smart office </a> </li>
      <li class="darkbluebg"> <a href="Sme-solutions.html">
        <div class="servicewhiteicon3"></div>
        Sme solutions </a> </li>
      <li> <a href="Professional-service.html">
        <div class="servicewhiteicon4"></div>
        Professional service </a> </li>
    </ul>
  </div>-->
  <div class="container-fluid">
    <div id="enquirymain">
      <div class="enquirybtn clearfix"> <span class="phoneicon"></span>
        <h4> QUICK<br/>
          CONTACT</h4>
      </div>
      <form action="qsend.php" class="enquiryform" method="post">
        <div class=" clearfix">
          <div class="form-group">
          <input type="text" class="form-control" id="name" name="name" pattern="[a-zA-Z][a-zA-Z.]+" title="Name" required placeholder="Name">
        </div>
        <div class="form-group">
          <input type="tel" class="form-control" id="phone" name="phone" pattern="[0-9,+][0-9]+" title="Phone No" required placeholder="Phone" >
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" name="email" required placeholder="E-mail" title="E-mail">
        </div>
        <div class="form-group">
          <textarea class="form-control2" rows="4" name="message"></textarea>
        </div>
          <span class="submit">
          <input class="btn" type="submit" value="Send">
          </span> </div>
      </form>
    </div>
  </div>
  <!--<div class="welcomearea">
    <div class="row">
      <div class="col-sm-3"> <img src="img/welcomeimg.jpg" class="img-responsive welcomeareaimg" alt=""/> </div>
      <div class="col-sm-9">
        <div class="welcometexarea">
          <h3>Welcome to</h3>
          <h2>Supernet Technology LLC</h2>
          <p>Supernet is proud to be one of the most successful & highly reputed IT service providers in the industry, delivering quality end to end support to companies across the globe. Be it cloud services, IT infrastructure, office automation, software development or smart communication systems, we ensure that your business requirements are well taken care of. </p>
          <div class="more"><a href="about.html">Read More</a></div>
        </div>
      </div>
    </div>
  </div>-->
</div>