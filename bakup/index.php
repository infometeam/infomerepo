<?php
include('header.php');
?>

<div class="container content" style="margin-bottom: 40px;">
	<h3 class="feat_head">featured products</h3>
	<div class="feat_wrap">
		<?php
			//$hmSelect = "select pro.ID as proID, pro.productCategoryID, pro.productName, pro.thumbnailPath, pro.hmFeat, pcat.ID as pcatID, pcat.productCategory from ".TABLE_PRODUCT." pro, ".TABLE_PRODUCT_CATEGORY." pcat where hmFeat = 1 and pro.productCategoryID = pcat.ID";
			$hmSelect = "select productCategoryID, productName, thumbnailPath, hmFeat from ".TABLE_PRODUCT." where hmFeat = 1";
			//echo $hmSelect;
			$hmQuery = mysqli_query($connection, $hmSelect);
			if(mysqli_num_rows($hmQuery) > 0){
				while($hmRow = mysqli_fetch_array($hmQuery)){
				?>				
				<div class="hm_pro_box">
					<div class="hm_pro_inner">
						<div class="hm_pro_img">
							<img src="admin/<?= $hmRow['thumbnailPath']; ?>"/>
							<a href="products.php?category=<?= $hmRow['productCategoryID']; ?>">
								<span>more</span>
							</a>
						</div>					
						<h3><?= $hmRow['productName']; ?></h3>
					</div>					
				</div>	
				<?php
				}
			}
		?>
		<div class="bd_clear"></div>
	</div>
</div>

<div class="homeservicearea">
  <div class="container">
    <ul class="clearfix">
      <li>
        <div class="media">
          <div class="media-left"> <img src="img/servicebluicon1.png" alt=""/> </div>
          <div class="media-body">
            <h4 class="media-heading"><a href="cloud.php">Cloud</a></h4>
            <p>The crux of a cloud service is based on the 3 crucial factors – Infrastructure, management & expertise. Supernet’s quality infrastructure support... </p>
          </div>
        </div>
      </li>
      <li>
        <div class="media">
          <div class="media-left"> <img src="img/servicebluicon2.png" alt=""/> </div>
          <div class="media-body">
            <h4 class="media-heading"><a href="Smart-office.php">Smart office</a></h4>
            <p>Businesses around the world are transforming like never before. And the role of technology in this revolution is something we need to appreciate. </p>
          </div>
        </div>
      </li>
      <li>
        <div class="media">
          <div class="media-left"> <img src="img/servicebluicon3.png" alt=""/> </div>
          <div class="media-body">
            <h4 class="media-heading"><a href="Sme-solutions.php">Sme solutions</a></h4>
            <p>To remain competitive and profitable, small and medium size organisations must constantly work to reduce operating costs and increase efficiency. </p>
          </div>
        </div>
      </li>
      <li>
        <div class="media">
          <div class="media-left"> <img src="img/servicebluicon4.png" alt=""/> </div>
          <div class="media-body">
            <h4 class="media-heading"><a href="Professional-service.php">Professional service</a></h4>
            <p>The challenges faced by different businesses are not always the same. As such, companies require a specific, customized solution that will help them... </p>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<div class="container">
  <div class="brandarea">
    <div class="owl-carousel" id="brands">
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand1.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand2.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand3.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand4.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand5.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand6.jpg" alt=""/></a></div>
      </div>
      <div class="item ">
        <div class="brandsimg"><a href="#"><img src="img/brand7.jpg" alt=""/></a></div>
      </div>
    </div>
  </div>
</div>
<?php
include('footer.php');
?>