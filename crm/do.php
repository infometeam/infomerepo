<?php
require("config/config.inc.php");
require("config/Database.class.php");
require("config/Application.class.php");
if (!$_POST['user_name'] || !$_POST['password']) {
    $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
    header("location:index.php");
} else {
    $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db -> connect();
    $userName = mysql_real_escape_string(htmlentities(trim($_POST['user_name'])));
    $password = md5(mysql_real_escape_string(htmlentities(trim($_POST['password']))));
    $qry = $db->query("SELECT * FROM `".TABLE_LOGIN."` WHERE user_name = '". $userName ."' AND password = '". $password ."'");
    if (mysql_num_rows($qry) > 0) {
        $row = mysql_fetch_array($qry);
        if ($row['status'] == 1) {
            $_SESSION['auth'] = array("id" => $row['id'], "user" => $row['user_name'], "type" => $row['type'], "employee_id" => $row['employee_id']);
            $db -> close();
            $redUrl = "cpanel/dashboard";
            if ($row['type'] == "employee") {
                $redUrl = "cpanel/employee-dashboard";
            }
            header("location: ".$redUrl);
        } else {
            $_SESSION['flash'] = $App->session_flash(0, "You are temporarily blocked by the admin.");
            $db -> close();
            header("location:index.php");
        }

    } else {
        $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
        $db -> close();
        header("location:index.php");
    }
    $db -> close();
}