<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
if (!$_POST['current_password'] || !$_POST['password'] || !$_POST['confirm_password']) {
    $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
    header("location:../change-password");
} else {
    if ($_POST['password'] != $_POST['confirm_password']) {
        $_SESSION['flash'] = $App->session_flash(0, "Error! Password mismatch. Please try again.");
        header("location:../change-password");
    } else {
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        $authId = $_SESSION['auth']['id'];
        $olPassword = md5(mysql_real_escape_string(htmlentities(trim($_POST['current_password']))));
        $qry = $db -> query("SELECT password from `". TABLE_LOGIN ."` WHERE id = '". $authId ."'");
        if (mysql_num_rows($qry) > 0) {
            $row = mysql_fetch_array($qry);
            if ($olPassword == $row['password']) {
                $data['password'] = md5(mysql_real_escape_string(htmlentities(trim($_POST['password']))));
                $status = $db -> query_update(TABLE_LOGIN, $data, "id = '{$authId}'");
                if ($status) {
                    $db -> close();
                    header('location: ../../logout.php');
                } else {
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    $db -> close();
                    header("location:../change-password");
                }
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
                $db -> close();
                header("location:../change-password");
            }
        } else {
            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
            $db -> close();
            header("location:../change-password");
        }
        $db -> close();
    }
}