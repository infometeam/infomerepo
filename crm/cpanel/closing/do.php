<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['customer_id'] || !$_POST['product'] || !$_POST['invoice_no'] || !$_POST['amount']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $closing = [
                "customer_id" => $App -> convert($_POST['customer_id']),
                "product" => $App -> convert($_POST['product']),
                "invoice_no" => $App -> convert($_POST['invoice_no']),
                "amount" => $App -> convert($_POST['amount']),
                "employee_id" => $_SESSION['auth']['employee_id']
            ];
            $status = $db -> query_insert(TABLE_CLOSING, $closing);
            if ($status) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(1, "Successfully added closing.");
                header("location: ../closing");
            } else {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: add.php");
            }
            $db -> close();
        }
        break;
    case "edit" :
        if (!$_POST['closing'] || !$_POST['customer_id'] || !$_POST['product'] || !$_POST['invoice_no'] || !$_POST['amount']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../closing");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $closingId = $_POST['closing'];
            $closing = [
                "customer_id" => $App -> convert($_POST['customer_id']),
                "product" => $App -> convert($_POST['product']),
                "invoice_no" => $App -> convert($_POST['invoice_no']),
                "amount" => $App -> convert($_POST['amount']),
                "employee_id" => $_SESSION['auth']['employee_id']
            ];
            $status = $db -> query_update(TABLE_CLOSING, $closing,"id = '". $closingId ."'");
            if ($status) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(1, "Meeting details updated successfully.");
                header("location: edit.php?closing=".$closingId);
            } else {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: add.php");
            }
            $db -> close();
        }
        break;
    case "delete" :
        if (!$_POST['closing']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../closing");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $closingId = $_POST['closing'];
            $status = 0;
            try {
                $status = @mysql_query("DELETE FROM `". TABLE_CLOSING ."` WHERE id = '". $closingId ."'");
            } catch (Exception $e) {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../closing");
            }
            if ($status) {
                $_SESSION['flash'] = $App->session_flash(1, "Closing details deleted successfully.");
                header("location: ../closing");
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../closing");
            }
            $db -> close();
        }
        break;
}