<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Closing
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Closing</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Closing Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $closingId = mysql_real_escape_string($_REQUEST['closing']);
                        $qry = $db -> query("SELECT cl.id as closing_id, cl.product, cl.invoice_no, cl.amount, c.id as customer_id, c.name FROM `". TABLE_CLOSING ."` cl INNER JOIN `". TABLE_CUSTOMER ."` c ON cl.customer_id = c.id WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."' AND cl.id = '". $closingId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post">
                            <input type="hidden" name="closing" value="<?= $closingId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="customer">Company Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="customer" data-role="trigger_customer_search" class="form-control" name="customer" required autocomplete="off" value="<?= $row['name']; ?>">
                                        <ul class="live_list"></ul>
                                        <input type="hidden" id="customer_id" class="form-control" name="customer_id" required value="<?= $row['customer_id']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="product">Product</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="product" class="form-control" name="product" value="<?= $row['product']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="invoice_no">Invoice No</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="invoice_no" class="form-control" name="invoice_no" value="<?= $row['invoice_no']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="amount">Amount</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="amount" class="form-control" name="amount" value="<?= $row['amount']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        $db -> close();
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");