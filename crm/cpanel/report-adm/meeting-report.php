<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Report
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Report</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        $condition = 1;
        if (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") {
            $condition = $condition. " AND c.name LIKE '%".@mysql_escape_string($_REQUEST['customer_name'])."%'";
        }
        if (isset($_REQUEST['contact_person']) && $_REQUEST['contact_person']!= "") {
            $condition = $condition. " AND c.contact_person LIKE '%".@mysql_escape_string($_REQUEST['contact_person'])."%'";
        }
        if (isset($_REQUEST['mobile']) && $_REQUEST['mobile']!= "") {
            $condition = $condition. " AND c.mobile LIKE '%".@mysql_escape_string($_REQUEST['mobile'])."%'";
        }
        if ((isset($_REQUEST['from']) && $_REQUEST['from']!= "") && (!isset($_REQUEST['to']) || $_REQUEST['to'] == "")) {
            $condition = $condition. " AND m.date >= '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['from'])) ."'";
        } else if ((isset($_REQUEST['to']) && $_REQUEST['to']!= "") && (!isset($_REQUEST['from']) || $_REQUEST['from'] == "")) {
            $condition = $condition." AND m.date <= '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['to'])) ."'";
        } else if (isset($_REQUEST['from']) && $_REQUEST['from']!= "" && isset($_REQUEST['to']) && $_REQUEST['to']!= "") {
            $condition = $condition. " AND m.date BETWEEN '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['from'])) ."' AND '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['to'])) ."'";
        }
        if (isset($_REQUEST['employee_user_name']) && $_REQUEST['employee_user_name']!= "") {
            $condition = $condition." AND l.user_name LIKE '%".@mysql_escape_string($_REQUEST['employee_user_name'])."%'";
        }

        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">Meeting Report</h3>
                        <div class="report_table_filter_wrapper clearfix">
                            <form class="table_filters report_table_filters clearfix" method="get">
                                <input class="form-control" name="customer_name" placeholder="Customer" type="text" value="<?= (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") ? $_REQUEST['customer_name'] : "" ?>">
                                <input class="form-control" name="contact_person" placeholder="Contact Person" type="text" value="<?= (isset($_REQUEST['contact_person']) && $_REQUEST['contact_person']!= "") ? $_REQUEST['contact_person'] : "" ?>">
                                <input class="form-control" name="mobile" placeholder="Mobile" type="text" value="<?= (isset($_REQUEST['mobile']) && $_REQUEST['mobile']!= "") ? $_REQUEST['mobile'] : "" ?>">
                                <input class="form-control date-picker" name="from" placeholder="From Date" type="text" value="<?= (isset($_REQUEST['from']) && $_REQUEST['from']!= "") ? $_REQUEST['from'] : "" ?>">
                                <input class="form-control date-picker" name="to" placeholder="To Date" type="text" value="<?= (isset($_REQUEST['to']) && $_REQUEST['to']!= "") ? $_REQUEST['to'] : "" ?>">
                                <input class="form-control" name="employee_user_name" placeholder="Employee" type="text" value="<?= (isset($_REQUEST['employee_user_name']) && $_REQUEST['employee_user_name']!= "") ? $_REQUEST['employee_user_name'] : "" ?>">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Added By</th>
                                <th>Customer</th>
                                <th>Contact Person</th>
                                <th>Mobile</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $qry = $db -> query("SELECT m.id as meeting_id, m.date, m.description as meeting_description, c.name, c.level, c.contact_person, c.designation, c.mobile, c.email, c.telephone, c.fax, c.po_box_no, c.description, c.remarks as customer_remarks, l.user_name FROM `". TABLE_MEETING ."` m INNER JOIN `". TABLE_CUSTOMER ."` c ON m.customer_id = c.id INNER JOIN `". TABLE_LOGIN ."` l ON l.employee_id = m.employee_id WHERE ".$condition);
                            if (mysql_num_rows($qry) > 0) {
                                $i = 1;
                                while ($row = mysql_fetch_array($qry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $App -> dbformat_date_db_with_hyphen($row['date']); ?></td>
                                        <td><?= $row['user_name']; ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#customer_det<?= $i; ?>"><?= $row['name']; ?></a>
                                            <div id="customer_det<?= $i; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Customer Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Company Name</th>
                                                                            <td><?= $row['name']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Level</th>
                                                                            <td><?= $row['level']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Contact Person</th>
                                                                            <td><?= $row['contact_person']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Designation</th>
                                                                            <td><?= $row['designation']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Mobile</th>
                                                                            <td><a href="tel:<?= $row['mobile']; ?>"><?= $row['mobile']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Email</th>
                                                                            <td><a href="mailto:<?= $row['email']; ?>"><?= $row['email']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Telephone</th>
                                                                            <td><a href="tel:<?= $row['telephone']; ?>"><?= $row['telephone']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Fax</th>
                                                                            <td><?= $row['fax']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Post Box No</th>
                                                                            <td><?= $row['po_box_no']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Description</th>
                                                                            <td><?= $row['description']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Remarks</th>
                                                                            <td><?= $row['customer_remarks']; ?></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-flat btn-primary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $row['contact_person']; ?></td>
                                        <td><?= $row['mobile']; ?></td>
                                        <td><?= $row['meeting_description']; ?></td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">No data found</td>
                                </tr>
                                <?php
                            }
                            $db -> close();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");