<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <?php
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        ?>
        <div class="row">
            <?php
            $countUnion = "SELECT COUNT(*) as total, 'Employees' as name, 'aqua' as color, 'ios-people' as icon, 'employee' as url FROM `".TABLE_EMPLOYEE."`
            UNION ALL
SELECT COUNT(*) as total, 'Customers' as name, 'green' as color, 'person-stalker' as icon, 'customer' as url FROM `".TABLE_CUSTOMER."`";
            $countQry = $db -> query($countUnion);
            if (mysql_num_rows($countQry) > 0) {
                while ($countRow = mysql_fetch_array($countQry)) {
                    ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-<?= $countRow['color']; ?>">
                            <div class="inner">
                                <h3><?= $countRow['total']; ?></h3>
                                <p><?= $countRow['name']; ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-<?= $countRow['icon']; ?>"></i>
                            </div>
                            <a href="../<?= $countRow['url']; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </section>
<?php
require ("../footer.php");