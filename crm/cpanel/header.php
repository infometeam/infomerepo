<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$dashboardUrl = "../dashboard";
if ($_SESSION['auth']['type'] == "employee") {
    $dashboardUrl = "../employee-dashboard";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Infome | Admin</title>
    <link rel="icon" href="../../img/logo.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
    <link rel="stylesheet" type="text/css" href="../../css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?= $dashboardUrl; ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <!--<span class="logo-mini"><b>I</b>n</span>-->
            <span class="logo-mini"><img src="../../img/logo.png" height="21px;" alt="In"></span>
            <!-- logo for regular state and mobile devices -->
            <!--<span class="logo-lg"><b>Infome</b></span>-->
            <span class="logo-lg"><img src="../../img/logo_text.png" alt="Infome"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span><?= $_SESSION['auth']['user'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="../change-password" class="btn btn-flat btn-default">Change Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="../../logout.php" class="btn btn-flat btn-default">
                                        <span>Logout</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../../img/default_user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?= $_SESSION['auth']['user']; ?></p>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->

            <ul class="sidebar-menu">
                <?php
                if ($_SESSION['auth']['type'] == 'admin') {
                ?>
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="<?= $dashboardUrl; ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="../employee">
                            <i class="fa fa-users"></i> <span>Employees</span>
                        </a>
                    </li>
                    <li>
                        <a href="../customer">
                            <i class="fa fa-user"></i> <span>Customers</span>
                        </a>
                    </li>
                    <li>
                        <a href="../supplier">
                            <i class="fa fa-user"></i> <span>Suppliers</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text"></i> <span>Report</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="../report-adm/customer-report.php"><i class="fa fa-circle-o"></i> Customer Report</a></li>
                            <li><a href="../report-adm/visit-report.php"><i class="fa fa-circle-o"></i> Visit Report</a></li>
                            <li><a href="../report-adm/meeting-report.php"><i class="fa fa-circle-o"></i> Meeting Report</a></li>
                            <li><a href="../report-adm/closing-report.php"><i class="fa fa-circle-o"></i> Closing Report</a></li>
                        </ul>
                    </li>
                <?php
                } else if ($_SESSION['auth']['type'] == 'employee') {
                    ?>
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="<?= $dashboardUrl; ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="../customer">
                            <i class="fa fa-user"></i> <span>Customers</span>
                        </a>
                    </li>
                    <li>
                        <a href="../visit">
                            <i class="fa fa-eye"></i> <span>Visits</span>
                        </a>
                    </li>
                    <li>
                        <a href="../meeting">
                            <i class="fa fa-bookmark"></i> <span>Meeting</span>
                        </a>
                    </li>
                    <li>
                        <a href="../closing">
                            <i class="fa fa-flag"></i> <span>Closing</span>
                        </a>
                    </li>
                    <li>
                        <a href="../supplier">
                            <i class="fa fa-user"></i> <span>Suppliers</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text"></i> <span>Report</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <!--<li><a href="../report-emp/customer-report.php"><i class="fa fa-circle-o"></i> Customer Report</a></li>
                            <li><a href="../report-emp/visit-report.php"><i class="fa fa-circle-o"></i> Visit Report</a></li>
                            <li><a href="../report-emp/meeting-report.php"><i class="fa fa-circle-o"></i> Meeting Report</a></li>
                            <li><a href="../report-emp/closing-report.php"><i class="fa fa-circle-o"></i> Closing Report</a></li>-->
                            <li><a href="../report-adm/customer-report.php"><i class="fa fa-circle-o"></i> Customer Report</a></li>
                            <li><a href="../report-adm/visit-report.php"><i class="fa fa-circle-o"></i> Visit Report</a></li>
                            <li><a href="../report-adm/meeting-report.php"><i class="fa fa-circle-o"></i> Meeting Report</a></li>
                            <li><a href="../report-adm/closing-report.php"><i class="fa fa-circle-o"></i> Closing Report</a></li>
                        </ul>
                    </li>
                    <?php
                }
                ?>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">