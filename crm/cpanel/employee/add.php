<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employees
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Employees</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Employee</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="do.php?op=new" method="post">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="name" class="form-control" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <textarea name="address" id="address" required class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="phone" class="form-control" name="contact_no" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="email" id="email" class="form-control" name="email" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="user_name">User Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="user_name" class="form-control" name="user_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" class="form-control" id="password" name="password" required autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" required autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");