<?php
require("../header.php");
/*if (!isset($_REQUEST['employee']) || !$_REQUEST['employee']) {
    header('Location: ../employee');
}*/
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Employees
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Employees</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Employee Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $employeeId = mysql_real_escape_string($_REQUEST['employee']);
                        $qry = $db -> query("SELECT * FROM `". TABLE_EMPLOYEE ."` WHERE id = '". $employeeId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post">
                            <input type="hidden" name="employee" value="<?= $employeeId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="name" class="form-control" name="name" value="<?= $row['name'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <textarea name="address" id="address" required class="form-control"><?= $row['address'] ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="phone" class="form-control" name="contact_no" value="<?= $row['contact_no'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="email" id="email" class="form-control" name="email" value="<?= $row['email'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                        <button type="button" data-toggle="modal" data-target="#reset_password_pop" class="btn btn-primary btn-flat">Reset Password</button>
                                    </div>
                                </div>
                                <!--<div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="user_name">User Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="user_name" class="form-control" name="user_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" class="form-control" id="password" name="password" required autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" required autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </div>-->
                            </div>
                        </form>
                        <?php
                        $db -> close();
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
    <div id="reset_password_pop" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="do.php?op=password-reset" method="post">
                    <input type="hidden" name="employee" value="<?= $employeeId; ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reset Employee Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="password">New Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                            <input type="password" class="form-control" id="password" name="password" required autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" required autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-flat btn-primary">Reset</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php
require ("../footer.php");