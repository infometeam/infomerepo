<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['name'] || !$_POST['user_name'] || !$_POST['password'] || !$_POST['confirm_password']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            if ($_POST['password'] != $_POST['confirm_password']) {
                $_SESSION['flash'] = $App->session_flash(0, "Error! Password mismatch. Please try again.");
                header("location: add.php");
            } else {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $userName = $App->convert($_POST['user_name']);
                $email = $App->convert($_POST['email']);
                $existenceLog = $db -> checkValueExist(TABLE_LOGIN, "user_name = '". $userName ."'");
                $existenceEmp = $db -> checkValueExist(TABLE_EMPLOYEE, "email = '". $email ."'");
                if ($existenceLog || $existenceEmp) {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Error! User name or email id already exists.");
                    header("location: add.php");
                } else {
                    $employee = array(
                        "name" => $App -> convert($_POST['name']),
                        "address" => $App -> convert($_POST['address']),
                        "contact_no" => $App -> convert($_POST['contact_no']),
                        "email" => $App -> convert($_POST['email'])
                    );
                    $employeeID = $db -> query_insert(TABLE_EMPLOYEE, $employee);
                    if ($employeeID) {
                        $login = array(
                            "employee_id" => $employeeID,
                            "user_name" => $App -> convert($_POST['user_name']),
                            "password" => md5(mysql_real_escape_string(htmlentities(trim($_POST['password'])))),
                            "type" => "employee"
                        );
                        $loginID = $db -> query_insert(TABLE_LOGIN, $login);
                        if ($loginID) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Employee added successfully.");
                            header("location: ../employee");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: add.php");
                        }
                    } else {
                        $db -> close();
                        $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                        header("location: add.php");
                    }
                }
                $db -> close();
            }
        }
        break;
    case "edit" :
        if (!$_POST['employee'] || !$_POST['name'] || !$_POST['email']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: edit.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $employeeID = $_POST['employee'];
            $status = $db -> query_update(TABLE_EMPLOYEE, ["name" => $App -> convert($_POST['name']), "address" => $App -> convert($_POST['address']), "contact_no" => $App -> convert($_POST['contact_no']), "email" => $App -> convert($_POST['email'])], "id = '". $employeeID ."'");
            if ($status) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(1, "Employee details updated successfully.");
                header("location: edit.php?employee=".$employeeID);
            } else {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: edit.php?employee=".$employeeID);
            }
            $db -> close();
        }
        break;
    case "block" :
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        $employeeID = $_POST['employee'];
        $status = $db -> query_update(TABLE_LOGIN, ["status" => 0], "employee_id = '". $employeeID ."'");
        if ($status) {
            $db -> close();
            $_SESSION['flash'] = $App->session_flash(1, "Employee blocked successfully.");
            header("location: ../employee");
        } else {
            $db -> close();
            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
            header("location: ../employee");
        }
        $db -> close();
        break;
    case "unblock" :
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        $employeeID = $_POST['employee'];
        $status = $db -> query_update(TABLE_LOGIN, ["status" => 1], "employee_id = '". $employeeID ."'");
        if ($status) {
            $db -> close();
            $_SESSION['flash'] = $App->session_flash(1, "Employee unblocked successfully.");
            header("location: ../employee");
        } else {
            $db -> close();
            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
            header("location: ../employee");
        }
        $db -> close();
        break;
    case "password-reset" :
        if (!$_POST['employee'] || !$_POST['password'] || !$_POST['confirm_password']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../employee");
        } else {
            $employeeID = $_POST['employee'];
            if ($_POST['password'] != $_POST['confirm_password']) {
                $_SESSION['flash'] = $App->session_flash(0, "Error! Password mismatch. Please try again.");
                header("location: edit.php?employee=".$employeeID);
            } else {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $status = $db -> query_update(TABLE_LOGIN, ["password" => md5(mysql_real_escape_string(htmlentities(trim($_POST['password']))))], "employee_id = '". $employeeID ."'");
                if ($status) {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(1, "Successfully changed employees password");
                    header("location: edit.php?employee=".$employeeID);
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong! Please try again.");
                    header("location: edit.php?employee=".$employeeID);
                }
                $db -> close();
            }
        }
        break;
    case "delete" :
        echo "delete";
        break;
}