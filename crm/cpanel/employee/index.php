<?php
require("../header.php");
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Employees
        <!--<small>Control panel</small>-->
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Employees</li>
    </ol>
</section>
<section class="content">
    <?php
    if (isset($_SESSION['flash'])) {
        echo $_SESSION['flash'];
        unset($_SESSION['flash']);
    }
    if (isset($_REQUEST['search']) && $_REQUEST['search']!= "") {
        $condition = "name LIKE '%".$_REQUEST['search']."%'";
    } else {
        $condition = 1;
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header clearfix">
                    <h3 class="box-title">List of Employees</h3>
                    <div class="table_filter_wrapper clearfix">
                        <form class="table_filters clearfix" method="get">
                            <input class="form-control" name="search" placeholder="Search" type="text" value="<?= (isset($_REQUEST['search']) && $_REQUEST['search']!= "") ? $_REQUEST['search'] : "" ?>">
                            <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                        </form>
                        <span class="pull-left"><a href="add.php" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>User Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $qry = $db -> query("SELECT e.id as employee_id, e.name, e.address, e.contact_no, e.email, l.user_name, l.status FROM `".TABLE_EMPLOYEE."` e INNER JOIN `".TABLE_LOGIN."` l ON e.id = l.employee_id WHERE ".$condition);
                        if (mysql_num_rows($qry) > 0) {
                            $i = 1;
                            while ($row = mysql_fetch_array($qry)) {
                                ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $row['name']; ?></td>
                                    <td><?= $row['address']; ?></td>
                                    <td><a href="tel:<?= $row['contact_no']; ?>"><?= $row['contact_no']; ?></a></td>
                                    <td><a href="mailto:<?= $row['email']; ?>"><?= $row['email']; ?></a></td>
                                    <td><?= $row['user_name']; ?></td>
                                    <td>
                                        <?php
                                        $op = "block";
                                        $btnText = "Block";
                                        $btnType = "warning";
                                        if ($row['status'] == 0) {
                                            $op = "unblock";
                                            $btnText = "Unblock";
                                            $btnType = "success";
                                        }
                                        ?>
                                        <a href="edit.php?employee=<?= $row['employee_id']; ?>" class="btn btn-flat btn-primary">Edit</a>
                                        <button data-role="trigger_form" data-behaviour="confirm" data-verify="Are you sure you want to <?= $op; ?> the employee?" class="btn btn-flat btn-<?= $btnType; ?>"><?= $btnText; ?>
                                            <form class="form_hidden" method="post" action="do.php">
                                                <input type="hidden" name="op" value="<?= $op; ?>">
                                                <input type="hidden" name="employee" value="<?= $row['employee_id']; ?>">
                                            </form>
                                        </button>
                                        <!--<button data-role="trigger_form" data-behaviour="confirm" data-verify="Are you sure you want to delete the employee?" class="btn btn-flat btn-danger">Delete
                                            <form class="form_hidden" method="post" action="do.php">
                                                <input type="hidden" name="op" value="delete">
                                                <input type="hidden" name="employee" value="<?/*= $row['employee_id']; */?>">
                                            </form>
                                        </button>-->
                                    </td>
                                </tr>
                                <?php
                                $i += 1;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="7" class="text-center">No data found</td>
                            </tr>
                            <?php
                        }
                        $db -> close();
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<?php
require ("../footer.php");