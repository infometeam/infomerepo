<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Meetings
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Meetings</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Meeting Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $meetingId = mysql_real_escape_string($_REQUEST['meeting']);
                        $qry = $db -> query("SELECT m.date, m.description as meeting_description, c.id as customer_id, c.name FROM `". TABLE_MEETING ."` m INNER JOIN `". TABLE_CUSTOMER ."` c ON m.customer_id = c.id WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."' AND m.id = '". $meetingId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post">
                            <input type="hidden" name="meeting" value="<?= $meetingId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="date-picker">Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="date-picker" class="form-control date-picker" name="date" value="<?= $App -> dbformat_date_db_with_hyphen($row['date']); ?>" required>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="customer">Company Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="customer" data-role="trigger_customer_search" class="form-control" name="customer" value="<?= $row['name']; ?>" required>
                                        <ul class="live_list"></ul>
                                        <input type="hidden" id="customer_id" class="form-control" name="customer_id" value="<?= $row['customer_id']; ?>"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"><?= $row['meeting_description']; ?></textarea>
                                    </div>
                                    <?php
                                    $remQry = $db -> query("SELECT * FROM `". TABLE_REMINDER ."` WHERE reference_id = '". $meetingId ."' AND type = 'meeting'");
                                    if (mysql_num_rows($remQry) > 0) {
                                        while ($remRow = mysql_fetch_array($remQry)) {
                                            ?>
                                            <input type="hidden" name="reminder_action" value="edit">
                                            <div class="form-group">
                                                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                                <input type="text" class="form-control date-picker" name="reminder_date" value="<?= $App -> dbformat_date_db_with_hyphen($remRow['date']); ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Reminder Note</label>
                                                <textarea name="reminder_description" class="form-control"><?= $remRow['description']; ?></textarea>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" name="reminder_action" value="new">
                                        <div class="form-group">
                                            <input type="checkbox" data-role="trigger_dynamic_form" data-source="#reminder_form" name="reminder"> <span class="checkbox_label">Add a reminder</span>
                                        </div>
                                        <div class="dynamic_elements">

                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        $db -> close();
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>


    <!-- Dynamic form to include -->
    <div class="dynamic_source hidden">
        <div id="reminder_form">
            <div class="form-group">
                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                <input type="text" class="form-control dynamic_datepicker" name="reminder_date" required>
            </div>
            <div class="form-group">
                <label>Reminder Note</label>
                <textarea name="reminder_description" class="form-control"></textarea>
            </div>
        </div>
    </div>
<?php
require ("../footer.php");