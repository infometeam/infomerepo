<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Meetings
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Meetings</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        if (isset($_REQUEST['search']) && $_REQUEST['search']!= "") {
            $condition = "name LIKE '%".$_REQUEST['search']."%'";
        } else {
            $condition = 1;
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">List of Meetings</h3>
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="get">
                                <input class="form-control" name="search" placeholder="Search" type="text" value="<?= (isset($_REQUEST['search']) && $_REQUEST['search']!= "") ? $_REQUEST['search'] : "" ?>">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                            <span class="pull-left"><a href="add.php" class="btn btn-primary btn-flat">Add New</a></span>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Company Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $qry = $db -> query("SELECT m.id as meeting_id, m.date, m.description as meeting_description, c.name, c.level, c.contact_person, c.designation, c.mobile, c.email, c.telephone, c.fax, c.po_box_no, c.description, c.remarks as customer_remarks FROM `". TABLE_MEETING ."` m INNER JOIN `". TABLE_CUSTOMER ."` c ON m.customer_id = c.id WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."' AND ".$condition);
                            if (mysql_num_rows($qry) > 0) {
                                $i = 1;
                                while ($row = mysql_fetch_array($qry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $App -> dbformat_date_db_with_hyphen($row['date']); ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#customer_det<?= $i; ?>"><?= $row['name']; ?></a>
                                            <div id="customer_det<?= $i; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Customer Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th>Company Name</th>
                                                                                <td><?= $row['name']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Level</th>
                                                                                <td><?= $row['level']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Contact Person</th>
                                                                                <td><?= $row['contact_person']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Designation</th>
                                                                                <td><?= $row['designation']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Mobile</th>
                                                                                <td><a href="tel:<?= $row['mobile']; ?>"><?= $row['mobile']; ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Email</th>
                                                                                <td><a href="mailto:<?= $row['email']; ?>"><?= $row['email']; ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Telephone</th>
                                                                                <td><a href="tel:<?= $row['telephone']; ?>"><?= $row['telephone']; ?></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Fax</th>
                                                                                <td><?= $row['fax']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Post Box No</th>
                                                                                <td><?= $row['po_box_no']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Description</th>
                                                                                <td><?= $row['description']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>Remarks</th>
                                                                                <td><?= $row['customer_remarks']; ?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-flat btn-primary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $row['meeting_description']; ?></td>
                                        <td>
                                            <a href="edit.php?meeting=<?= $row['meeting_id']; ?>" class="btn btn-flat btn-primary">Edit</a>
                                            <button data-role="trigger_form" data-behaviour="confirm" data-verify="Are you sure you want to delete the employee?" class="btn btn-flat btn-danger">Delete
                                                <form class="form_hidden" method="post" action="do.php">
                                                    <input type="hidden" name="op" value="delete">
                                                    <input type="hidden" name="meeting" value="<?= $row['meeting_id']; ?>">
                                                </form>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">No data found</td>
                                </tr>
                                <?php
                            }
                            $db -> close();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");