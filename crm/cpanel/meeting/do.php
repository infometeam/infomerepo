<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$regexDate = "/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/";
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['customer_id']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            if (preg_match($regexDate, $_POST['date'])) {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $meeting = [
                    "date" => $App -> dbformat_date_db_with_hyphen($_POST['date']),
                    "customer_id" => $App -> convert($_POST['customer_id']),
                    "description" => $App -> convert($_POST['description']),
                    "employee_id" => $_SESSION['auth']['employee_id']
                ];
                $status = $db -> query_insert(TABLE_MEETING, $meeting);
                if ($status) {
                    if (isset($_POST['reminder'])) {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "meeting",
                            "reference_id" => $status
                        ];
                        $remStatus = $db -> query_insert(TABLE_REMINDER, $reminder);
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Successfully added visit.");
                            header("location: ../meeting");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: add.php");
                        }
                    } else {
                        $db -> close();
                        $_SESSION['flash'] = $App->session_flash(1, "Successfully added meeting.");
                        header("location: ../meeting");
                    }
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: add.php");
                }
                $db -> close();
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Please select a valid date!");
                header("location: add.php");
            }

        }
        break;
    case "edit" :
        if (!$_POST['meeting'] || !$_POST['customer_id']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../meeting");
        } else {
            if (preg_match($regexDate, $_POST['date'])) {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $meetingId = $_POST['meeting'];
                $meeting = [
                    "date" => $App -> dbformat_date_db_with_hyphen($_POST['date']),
                    "customer_id" => $App -> convert($_POST['customer_id']),
                    "description" => $App -> convert($_POST['description']),
                    "employee_id" => $_SESSION['auth']['employee_id']
                ];
                $status = $db -> query_update(TABLE_MEETING, $meeting,"id = '". $meetingId ."'");
                if ($status) {
                    if ($_POST['reminder_action'] == "new" && isset($_POST['reminder'])) {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "meeting",
                            "reference_id" => $meetingId
                        ];
                        $remStatus = $db -> query_insert(TABLE_REMINDER, $reminder);
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Meeting details updated successfully.");
                            header("location: ../meeting");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: add.php");
                        }
                    } else if ($_POST['reminder_action'] == "edit") {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "meeting",
                            "reference_id" => $meetingId
                        ];
                        $remStatus = $db -> query_update(TABLE_REMINDER, $reminder, "reference_id = '". $meetingId ."' AND type = 'meeting'");
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Meeting details updated successfully.");
                            header("location: ../meeting");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: add.php");
                        }
                    } else {
                        $db -> close();
                        $_SESSION['flash'] = $App->session_flash(1, "Meeting details updated successfully.");
                        header("location: edit.php?meeting=".$meetingId);
                    }
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: edit.php?meeting=".$meetingId);
                }
                $db -> close();
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Please select a valid date");
                header("location: edit.php?meeting=".$_POST['meeting']);
            }
            
        }
        break;
    case "delete" :
        if (!$_POST['meeting']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../meeting");
        } else {
            $meetingId = $_POST['meeting'];
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $remStatus = 0;
            $status = 0;
            $remQry = $db -> query("SELECT COUNT(*) as total FROM `". TABLE_REMINDER ."` WHERE type = 'meeting' AND reference_id = '". $meetingId ."'");
            $remCount = mysql_fetch_array($remQry);
            if ($remCount['total'] > 0) {
                try {
                    $remStatus = @mysql_query("DELETE FROM `". TABLE_REMINDER ."` WHERE type = 'meeting' AND reference_id = '". $meetingId ."'");
                } catch (Exception $e) {
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: ../meeting");
                }
            } else {
                $remStatus = 1;
            }
            try {
                $status = @mysql_query("DELETE FROM `". TABLE_MEETING."` WHERE id = '". $meetingId ."'");
            } catch (Exception $e) {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../meeting");
            }
            if ($status && $remStatus) {
                $_SESSION['flash'] = $App->session_flash(1, "Meeting details deleted successfully.");
                header("location: ../meeting");
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../meeting");
            }
            $db -> close();
        }
        break;
}