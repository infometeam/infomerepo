<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Meetings
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Meetings</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Meeting</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="do.php?op=new" method="post">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="date-picker">Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="date-picker" class="form-control date-picker" name="date" value="<?= date('d-m-Y'); ?>" required>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="customer">Company Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="customer" data-role="trigger_customer_search" class="form-control" name="customer" required autocomplete="off">
                                        <ul class="live_list"></ul>
                                        <input type="hidden" id="customer_id" class="form-control" name="customer_id" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" data-role="trigger_dynamic_form" data-source="#reminder_form" name="reminder"> <span class="checkbox_label">Add a reminder</span>
                                    </div>
                                    <div class="dynamic_elements">

                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>

    <!-- Dynamic form to include -->
    <div class="dynamic_source hidden">
        <div id="reminder_form">
            <div class="form-group">
                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                <input type="text" class="form-control dynamic_datepicker" name="reminder_date" required>
            </div>
            <div class="form-group">
                <label>Reminder Note</label>
                <textarea name="reminder_description" class="form-control"></textarea>
            </div>
        </div>
    </div>
<?php
require ("../footer.php");