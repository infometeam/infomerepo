<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Supplier
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Supplier</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Supplier Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $supplierId = mysql_real_escape_string($_REQUEST['supplier']);
                        $qry = $db -> query("SELECT * FROM `". TABLE_SUPPLIER ."` WHERE id = '". $supplierId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post">
                            <input name="supplier" type="hidden" value="<?= $supplierId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Name</label> <span class="text-danger">(Required)</span>
                                        <input type="text" id="name" class="form-control" name="name" value="<?= $row['name']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact_person">Contact Person</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="contact_person" class="form-control" name="contact_person" value="<?= $row['contact_person']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="telephone">Telephone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="telephone" class="form-control" name="telephone" value="<?= $row['telephone']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label></span>
                                        <input type="email" id="email" class="form-control" name="email" value="<?= $row['email']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="products">Products</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="products" class="form-control" name="products" value="<?= $row['products']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>

    <!-- Dynamic form to include -->
    <div class="dynamic_source hidden">
        <div id="reminder_form">
            <div class="form-group">
                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                <input type="text" class="form-control dynamic_datepicker" name="reminder_date" required>
            </div>
            <div class="form-group">
                <label>Reminder Note</label>
                <textarea name="reminder_description" class="form-control"></textarea>
            </div>
        </div>
    </div>
<?php
require ("../footer.php");