<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Suppliers
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Suppliers</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        if (isset($_REQUEST['search']) && $_REQUEST['search']!= "") {
            $condition = "name LIKE '%".$_REQUEST['search']."%'";
        } else {
            $condition = 1;
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">List of Suppliers</h3>
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="get">
                                <input class="form-control" name="search" placeholder="Search" type="text" value="<?= (isset($_REQUEST['search']) && $_REQUEST['search']!= "") ? $_REQUEST['search'] : "" ?>">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                            <span class="pull-left"><a href="add.php" class="btn btn-primary btn-flat">Add New</a></span>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Supplier Name</th>
                                <th>Contact Person</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Products</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $qry = $db -> query("SELECT s.id as supplier_id, s.name, s.contact_person, s.telephone, s.email, s.products, l.user_name FROM `". TABLE_SUPPLIER ."` s INNER JOIN `". TABLE_LOGIN ."` l ON s.log_id = l.id WHERE ".$condition);
                            if (mysql_num_rows($qry) > 0) {
                                $i = 1;
                                while ($row = mysql_fetch_array($qry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $row['name']; ?></td>
                                        <td><?= $row['contact_person']; ?></td>
                                        <td><a href="tel:<?= $row['telephone']; ?>"><?= $row['telephone']; ?></a></td>
                                        <td><a href="mailto:<?= $row['email']; ?>"><?= $row['email']; ?></a></td>
                                        <td><?= $row['products']; ?></td>
                                        <td>
                                            <a href="edit.php?supplier=<?= $row['supplier_id']; ?>" class="btn btn-flat btn-primary">Edit</a>
                                            <button data-role="trigger_form" data-behaviour="confirm" data-verify="Are you sure you want to delete the supplier?" class="btn btn-flat btn-danger">Delete
                                                <form class="form_hidden" method="post" action="do.php">
                                                    <input type="hidden" name="op" value="delete">
                                                    <input type="hidden" name="supplier" value="<?= $row['supplier_id']; ?>">
                                                </form>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="6" class="text-center">No data found</td>
                                </tr>
                                <?php
                            }
                            $db -> close();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");