<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['name'] || !$_POST['contact_person'] || !$_POST['telephone'] || !$_POST['products']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();

            $existence = $db -> checkValueExist(TABLE_SUPPLIER, "name = '".$_POST['name']."'");
            if ($existence) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Error! Supplier name already exists.");
                header("location: add.php");
            } else {
                $supplier = [
                    "name" => $App -> convert($_POST['name']),
                    "contact_person" => $App -> convert($_POST['contact_person']),
                    "telephone" => $App -> convert($_POST['telephone']),
                    "email" => $App -> convert($_POST['email']),
                    "products" => $App -> convert($_POST['products']),
                    "log_id" => $_SESSION['auth']['id']
                ];
                $status = $db -> query_insert(TABLE_SUPPLIER, $supplier);
                if ($status) {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(1, "Added new supplier successfully.");
                    header("location: ../supplier");
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: add.php");
                }
            }
            $db -> close();
        }
        break;
    case "edit" :
        if (!$_POST['name'] || !$_POST['contact_person'] || !$_POST['telephone'] || !$_POST['products'] || !$_POST['supplier']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $supplierId = $_REQUEST['supplier'];
            $supplier = [
                "name" => $App -> convert($_POST['name']),
                "contact_person" => $App -> convert($_POST['contact_person']),
                "telephone" => $App -> convert($_POST['telephone']),
                "email" => $App -> convert($_POST['email']),
                "products" => $App -> convert($_POST['products'])
            ];
            $status = $db -> query_update(TABLE_SUPPLIER, $supplier,"id = '". $supplierId ."'");
            if ($status) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(1, "Supplier details updated successfully.");
                header("location: edit.php?supplier=".$supplierId);
            } else {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: edit.php?supplier=".$supplierId);
            }
            $db -> close();
        }
        break;
    case "delete" :
        if (!$_POST['supplier']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../supplier");
        } else {
            $supplierId = $_REQUEST['supplier'];
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $status = 0;
            try {
                $status = @mysql_query("DELETE FROM `". TABLE_SUPPLIER ."` WHERE id = '". $supplierId ."'");
            } catch (Exception $e) {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../supplier");
            }
            if ($status) {
                $_SESSION['flash'] = $App->session_flash(1, "Supplier details deleted successfully.");
                header("location: ../supplier");
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../supplier");
            }
            $db -> close();
        }
        break;
}