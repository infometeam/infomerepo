<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reminders
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Reminders</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        $condition = 1;
        if (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") {
            $condition = $condition. " AND c.name LIKE '%".@mysql_escape_string($_REQUEST['customer_name'])."%'";
        }
        if ((isset($_REQUEST['from']) && $_REQUEST['from']!= "") && (!isset($_REQUEST['to']) || $_REQUEST['to'] == "")) {
            $condition = $condition. " AND r.date >= '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['from'])) ."'";
        } else if ((isset($_REQUEST['to']) && $_REQUEST['to']!= "") && (!isset($_REQUEST['from']) || $_REQUEST['from'] == "")) {
            $condition = $condition." AND r.date <= '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['to'])) ."'";
        } else if (isset($_REQUEST['from']) && $_REQUEST['from']!= "" && isset($_REQUEST['to']) && $_REQUEST['to']!= "") {
            $condition = $condition. " AND r.date BETWEEN '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['from'])) ."' AND '". $App -> dbformat_date_db_with_hyphen(@mysql_escape_string($_REQUEST['to'])) ."'";
        }
        if (isset($_REQUEST['employee_user_name']) && $_REQUEST['employee_user_name']!= "") {
            $condition = $condition." AND l.user_name LIKE '%".@mysql_escape_string($_REQUEST['employee_user_name'])."%'";
        }

        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">Reminder List</h3>
                        <div class="report_table_filter_wrapper clearfix">
                            <form class="table_filters report_table_filters clearfix" method="get">
                                <input class="form-control" name="customer_name" placeholder="Customer" type="text" value="<?= (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") ? $_REQUEST['customer_name'] : "" ?>">
                                <input class="form-control date-picker" name="from" placeholder="From Date" type="text" value="<?= (isset($_REQUEST['from']) && $_REQUEST['from']!= "") ? $_REQUEST['from'] : "" ?>">
                                <input class="form-control date-picker" name="to" placeholder="To Date" type="text" value="<?= (isset($_REQUEST['to']) && $_REQUEST['to']!= "") ? $_REQUEST['to'] : "" ?>">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Reminder</th>
                            </tr>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $remQry = $db -> query("SELECT r.date, r.description as reminder_description, c.name, c.level, c.contact_person, c.designation, c.mobile, c.email, c.telephone, c.fax, c.po_box_no, c.description as customer_description, c.remarks, c.log_id, l.user_name FROM `". TABLE_REMINDER ."` r INNER JOIN `". TABLE_CUSTOMER ."` c ON r.customer_id = c.id INNER JOIN `". TABLE_LOGIN ."` l ON c.log_id = l.id WHERE r.employee_id = '". $_SESSION['auth']['employee_id'] ."' AND r.date >= '". date('Y-m-d') ."' AND ". $condition ." ORDER BY r.date ASC");
                            if (mysql_num_rows($remQry) > 0) {
                                $i = 1;
                                while ($reminder = mysql_fetch_array($remQry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $App -> dbformat_date_db_with_hyphen($reminder['date']); ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#customer_det<?= $i; ?>"><?= $reminder['name']; ?></a>
                                            <div id="customer_det<?= $i; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Customer Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Company Name</th>
                                                                            <td><?= $reminder['name']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Employee</th>
                                                                            <td><?= $reminder['user_name']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Level</th>
                                                                            <td><?= $reminder['level']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Contact Person</th>
                                                                            <td><?= $reminder['contact_person']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Designation</th>
                                                                            <td><?= $reminder['designation']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Mobile</th>
                                                                            <td><a href="tel:<?= $reminder['mobile']; ?>"><?= $reminder['mobile']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Email</th>
                                                                            <td><a href="mailto:<?= $reminder['email']; ?>"><?= $reminder['email']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Telephone</th>
                                                                            <td><a href="tel:<?= $reminder['telephone']; ?>"><?= $reminder['telephone']; ?></a></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Fax</th>
                                                                            <td><?= $reminder['fax']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Post Box No</th>
                                                                            <td><?= $reminder['po_box_no']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Description</th>
                                                                            <td><?= $reminder['customer_description']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Remarks</th>
                                                                            <td><?= $reminder['remarks']; ?></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-flat btn-primary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $reminder['reminder_description']; ?></td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="4">No reminders found.</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");