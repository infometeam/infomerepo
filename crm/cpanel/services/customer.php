<?php
header("Content-type: Application/json");
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
$api = ["status" => 1, "count" => 0];
if (!isset($_SESSION['auth']) || !$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    $api['status'] = 0;
    $api['error'] = "Request not authorized.";
} else {
    if (!isset($_POST['searchKey']) || !$_POST['searchKey']) {
        $api['status'] = 0;
        $api['error'] = "Search key is missing in the request.";
    } else {
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        $searchKey = $_POST['searchKey'];
        $qry = $db -> query("SELECT * FROM `". TABLE_CUSTOMER ."` WHERE name LIKE '%". $searchKey ."%'");
        $count = mysql_num_rows($qry);
        if ($count > 0) {
            $api['count'] = $count;
            $api['customers'] = [];
            while ($row = mysql_fetch_array($qry)) {
                array_push($api["customers"], $row);
            }
            $db -> close();
        } else {
            $db -> close();
            $api['status'] = 0;
            $api['error'] = "No data found.";
        }
    }
}
echo json_encode($api);