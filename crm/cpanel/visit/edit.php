<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Visits
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Visits</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Visit Details</h3>
                        <a href="#" class="btn btn-flat btn-primary btn-sm pull-right" data-toggle="modal" data-target="#customer_live">Add New Customer</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $visitId = mysql_real_escape_string($_REQUEST['visit']);
                        $qry = $db -> query("SELECT v.date, v.remarks as visit_remarks, c.id as customer_id, c.name FROM `". TABLE_VISIT ."` v INNER JOIN `". TABLE_CUSTOMER ."` c ON v.customer_id = c.id WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."' AND v.id = '". $visitId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post" data-listen="customer_live">
                            <input type="hidden" name="visit" value="<?= $visitId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="date-picker">Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="date-picker" class="form-control date-picker" name="date" value="<?= $App -> dbformat_date_db_with_hyphen($row['date']); ?>" required>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="customer">Company Name</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="customer" data-role="trigger_customer_search" class="form-control" name="customer" value="<?= $row['name']; ?>" required>
                                        <ul class="live_list"></ul>
                                        <input type="hidden" id="customer_id" class="form-control" name="customer_id" value="<?= $row['customer_id']; ?>"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea name="remarks" id="remarks" class="form-control"><?= $row['visit_remarks']; ?></textarea>
                                    </div>
                                    <?php
                                    $remQry = $db -> query("SELECT * FROM `". TABLE_REMINDER ."` WHERE reference_id = '". $visitId ."' AND type = 'visit'");
                                    if (mysql_num_rows($remQry) > 0) {
                                        while ($remRow = mysql_fetch_array($remQry)) {
                                            ?>
                                            <input type="hidden" name="reminder_action" value="edit">
                                            <div class="form-group">
                                                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                                <input type="text" class="form-control date-picker" name="reminder_date" value="<?= $App -> dbformat_date_db_with_hyphen($remRow['date']); ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Reminder Note</label>
                                                <textarea name="reminder_description" class="form-control"><?= $remRow['description']; ?></textarea>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input type="hidden" name="reminder_action" value="new">
                                        <div class="form-group">
                                            <input type="checkbox" data-role="trigger_dynamic_form" data-source="#reminder_form" name="reminder"> <span class="checkbox_label">Add a reminder</span>
                                        </div>
                                        <div class="dynamic_elements">

                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        $db -> close();
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>

    <!-- Dynamic form to include -->
    <div class="dynamic_source hidden">
        <div id="reminder_form">
            <div class="form-group">
                <label>Reminder Date</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                <input type="text" class="form-control dynamic_datepicker" name="reminder_date" required>
            </div>
            <div class="form-group">
                <label>Reminder Note</label>
                <textarea name="reminder_description" class="form-control"></textarea>
            </div>
        </div>
    </div>

    <!-- Customer live modal form -->
    <div id="customer_live" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Customer</h4>
                </div>
                <div class="modal-body">
                    <form action="do.php?op=live" method="post" class="customer_live_form">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="name">Company</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <input type="text" id="name" class="form-control" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="level">Level</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <select class="form-control" name="level">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="contact_person">Contact Person</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <input type="text" id="contact_person" class="form-control" name="contact_person" required>
                                </div>
                                <div class="form-group">
                                    <label for="designation">Designation</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <input type="text" id="designation" class="form-control" name="designation" required>
                                </div>
                                <div class="form-group">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" id="mobile" class="form-control" name="mobile">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <input type="email" id="email" class="form-control" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="telephone">Telephone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                    <input type="text" id="telephone" class="form-control" name="telephone" required>
                                </div>
                            </div>
                            <div class="col-lg-6col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="fax">Fax</label>
                                    <input type="text" id="fax" class="form-control" name="fax">
                                </div>
                                <div class="form-group">
                                    <label for="po_box_no">Post Box No</label>
                                    <input type="text" id="po_box_no" class="form-control" name="po_box_no">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="remarks">Remarks</label>
                                    <textarea name="remarks" id="remarks" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="trigger_customer_live" class="btn btn-flat btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
<?php
require ("../footer.php");