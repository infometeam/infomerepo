<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$regexDate = "/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/";
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['customer_id'] || !$_POST['date']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            if (preg_match($regexDate, $_POST['date'])) {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $visit = [
                    "date" => $App -> dbformat_date_db_with_hyphen($_POST['date']),
                    "customer_id" => $App -> convert($_POST['customer_id']),
                    "remarks" => $App -> convert($_POST['remarks']),
                    "employee_id" => $_SESSION['auth']['employee_id']
                ];
                $status = $db -> query_insert(TABLE_VISIT, $visit);
                if ($status) {
                    if (isset($_POST['reminder'])) {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "visit",
                            "reference_id" => $status
                        ];
                        $remStatus = $db -> query_insert(TABLE_REMINDER, $reminder);
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Successfully added visit.");
                            header("location: ../visit");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: add.php");
                        }
                    } else {
                        $db -> close();
                        $_SESSION['flash'] = $App->session_flash(1, "Successfully added visit.");
                        header("location: ../visit");
                    }
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: add.php");
                }
                $db -> close();
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Please select a valid date!");
                header("location: add.php");
            }

        }
        break;
    case "edit" :
        if (!$_POST['visit'] || !$_POST['customer_id'] || !$_POST['date']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../visit");
        } else {
            if (preg_match($regexDate, $_POST['date'])) {
                $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                $db -> connect();
                $visitId = $_POST['visit'];
                $visit = [
                    "date" => $App -> dbformat_date_db_with_hyphen($_POST['date']),
                    "customer_id" => $App -> convert($_POST['customer_id']),
                    "remarks" => $App -> convert($_POST['remarks']),
                    "employee_id" => $_SESSION['auth']['employee_id']
                ];
                $status = $db -> query_update(TABLE_VISIT, $visit,"id = '". $visitId ."'");
                if ($status) {
                    if ($_POST['reminder_action'] == "new" && isset($_POST['reminder'])) {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "visit",
                            "reference_id" => $visitId
                        ];
                        $remStatus = $db -> query_insert(TABLE_REMINDER, $reminder);
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Visit details updated successfully.");
                            header("location: ../visit");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: edit.php?visit=".$visitId);
                        }
                    } else if ($_POST['reminder_action'] == "edit") {
                        $reminder = [
                            "date" => $App -> dbformat_date_db_with_hyphen($_POST['reminder_date']),
                            "description" => $App -> convert($_POST['reminder_description']),
                            "employee_id" => $_SESSION['auth']['employee_id'],
                            "customer_id" => $App -> convert($_POST['customer_id']),
                            "type" => "visit",
                            "reference_id" => $visitId
                        ];
                        $remStatus = $db -> query_update(TABLE_REMINDER, $reminder, "reference_id = '". $visitId ."' AND type = 'visit'");
                        if ($remStatus) {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(1, "Visit details updated successfully.");
                            header("location: ../visit");
                        } else {
                            $db -> close();
                            $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                            header("location: edit.php?visit=".$visitId);
                        }
                    } else {
                        $db -> close();
                        $_SESSION['flash'] = $App->session_flash(1, "Visit details updated successfully.");
                        header("location: edit.php?visit=".$visitId);
                    }
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: edit.php?visit=".$visitId);
                }
                $db -> close();
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Please select a valid date!");
                header("location: edit.php?visit=".$_POST['visit']);
            }
        }
        break;
    case "delete" :
        if (!$_POST['visit']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: ../visit");
        } else {
            $visitId = $_POST['visit'];
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $remStatus = 0;
            $status = 0;
            $remQry = $db -> query("SELECT COUNT(*) as total FROM `". TABLE_REMINDER ."` WHERE type = 'visit' AND reference_id = '". $visitId ."'");
            $remCount = mysql_fetch_array($remQry);
            if ($remCount['total'] > 0) {
                try {
                    $remStatus = @mysql_query("DELETE FROM `". TABLE_REMINDER ."` WHERE type = 'visit' AND reference_id = '". $visitId ."'");
                } catch (Exception $e) {
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: ../visit");
                }
            } else {
                $remStatus = 1;
            }
            try {
                $status = @mysql_query("DELETE FROM `". TABLE_VISIT ."` WHERE id = '". $visitId ."'");
            } catch (Exception $e) {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../visit");
            }
            if ($status && $remStatus) {
                $_SESSION['flash'] = $App->session_flash(1, "Visit details deleted successfully.");
                header("location: ../visit");
            } else {
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: ../visit");
            }
            $db -> close();
        }
        break;
}