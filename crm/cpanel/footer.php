</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Powered By <a href="http://bodhiinfo.com/" target="_blank"><img src="../../img/bodhi_logo.png" alt="Bodhi"></a>
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="<?= $dashboardUrl; ?>">Infome</a>.</strong> All rights reserved.
</footer>

<!-- ./wrapper -->

<!-- Ajax Loader -->
<div class="loader_overlay">
    <div class="loader_wrapper">
        <svg width='200px' height='200px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-balls"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><g transform="rotate(0 50 50)">
                <circle r="7" cx="30" cy="50">
                    <animateTransform attributeName="transform" type="translate" begin="0s" repeatCount="indefinite" dur="1s" values="0 0;19.999999999999996 -20" keyTimes="0;1"/>
                    <animate attributeName="fill" dur="1s" begin="0s" repeatCount="indefinite"  keyTimes="0;1" values="#514134;#e35839"/>
                </circle>
            </g><g transform="rotate(90 50 50)">
                <circle r="7" cx="30" cy="50">
                    <animateTransform attributeName="transform" type="translate" begin="0s" repeatCount="indefinite" dur="1s" values="0 0;19.999999999999996 -20" keyTimes="0;1"/>
                    <animate attributeName="fill" dur="1s" begin="0s" repeatCount="indefinite"  keyTimes="0;1" values="#e35839;#d28d4f"/>
                </circle>
            </g><g transform="rotate(180 50 50)">
                <circle r="7" cx="30" cy="50">
                    <animateTransform attributeName="transform" type="translate" begin="0s" repeatCount="indefinite" dur="1s" values="0 0;19.999999999999996 -20" keyTimes="0;1"/>
                    <animate attributeName="fill" dur="1s" begin="0s" repeatCount="indefinite"  keyTimes="0;1" values="#d28d4f;#dbae1d"/>
                </circle>
            </g><g transform="rotate(270 50 50)">
                <circle r="7" cx="30" cy="50">
                    <animateTransform attributeName="transform" type="translate" begin="0s" repeatCount="indefinite" dur="1s" values="0 0;19.999999999999996 -20" keyTimes="0;1"/>
                    <animate attributeName="fill" dur="1s" begin="0s" repeatCount="indefinite"  keyTimes="0;1" values="#dbae1d;#514134"/>
                </circle>
            </g></svg>
    </div>
</div>

<!-- jQuery 2.2.4 -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
<script src="../../js/jquery-2.2.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!--<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>-->
<!-- Bootstrap 3.3.7 -->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->
<script src="../../js/bootstrap.min.js"></script>
<!-- datepicker -->
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE App -->
<script src="../../js/app.min.js"></script>

<script src="../../js/custom.js"></script>
</body>
</html>