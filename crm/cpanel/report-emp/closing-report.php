<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Report
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Report</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        $condition = 1;
        if (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") {
            $condition = $condition. " AND c.name LIKE '%".@mysql_escape_string($_REQUEST['customer_name'])."%'";
        }
        if (isset($_REQUEST['contact_person']) && $_REQUEST['contact_person']!= "") {
            $condition = $condition. " AND c.contact_person LIKE '%".@mysql_escape_string($_REQUEST['contact_person'])."%'";
        }
        if (isset($_REQUEST['mobile']) && $_REQUEST['mobile']!= "") {
            $condition = $condition. " AND c.mobile LIKE '%".@mysql_escape_string($_REQUEST['mobile'])."%'";
        }

        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">Meeting Report</h3>
                        <div class="pull-right clearfix">
                            <form class="table_filters clearfix" method="get">
                                <input class="form-control" name="customer_name" placeholder="Customer" type="text" value="<?= (isset($_REQUEST['customer_name']) && $_REQUEST['customer_name']!= "") ? $_REQUEST['customer_name'] : "" ?>">
                                <input class="form-control" name="contact_person" placeholder="Contact Person" type="text" value="<?= (isset($_REQUEST['contact_person']) && $_REQUEST['contact_person']!= "") ? $_REQUEST['contact_person'] : "" ?>">
                                <input class="form-control" name="mobile" placeholder="Mobile" type="text" value="<?= (isset($_REQUEST['mobile']) && $_REQUEST['mobile']!= "") ? $_REQUEST['mobile'] : "" ?>">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                            <span class="pull-left"><a href="add.php" class="btn btn-primary btn-flat">Add New</a></span>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer</th>
                                <th>Contact Person</th>
                                <th>Mobile</th>
                                <th>Product</th>
                                <th>Invoice No</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $qry = $db -> query("SELECT cl.id as closing_id, cl.product, cl.invoice_no, cl.amount, c.name, c.level, c.contact_person, c.designation, c.mobile, c.email, c.telephone, c.fax, c.po_box_no, c.description, c.remarks as customer_remarks FROM `". TABLE_CLOSING ."` cl INNER JOIN `". TABLE_CUSTOMER ."` c ON cl.customer_id = c.id WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."' AND ".$condition);
                            if (mysql_num_rows($qry) > 0) {
                                $i = 1;
                                while ($row = mysql_fetch_array($qry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#customer_det<?= $i; ?>"><?= $row['name']; ?></a>
                                            <div id="customer_det<?= $i; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Customer Details</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <td><?= $row['name']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Level</th>
                                                                            <td><?= $row['level']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Contact Person</th>
                                                                            <td><?= $row['contact_person']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Designation</th>
                                                                            <td><?= $row['designation']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Mobile</th>
                                                                            <td><?= $row['mobile']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Email</th>
                                                                            <td><?= $row['email']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Telephone</th>
                                                                            <td><?= $row['telephone']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Fax</th>
                                                                            <td><?= $row['fax']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Post Box No</th>
                                                                            <td><?= $row['po_box_no']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Description</th>
                                                                            <td><?= $row['description']; ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Remarks</th>
                                                                            <td><?= $row['customer_remarks']; ?></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-flat btn-primary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $row['contact_person']; ?></td>
                                        <td><?= $row['mobile']; ?></td>
                                        <td><?= $row['product']; ?></td>
                                        <td><?= $row['invoice_no']; ?></td>
                                        <td><?= $row['amount']; ?></td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">No data found</td>
                                </tr>
                                <?php
                            }
                            $db -> close();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");