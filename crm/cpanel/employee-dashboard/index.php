<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <?php
        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db -> connect();
        ?>
        <div class="row">
            <?php
            $whereCondition = "WHERE employee_id = '". $_SESSION['auth']['employee_id'] ."'";
            $countUnion = "SELECT COUNT(*) as total, 'Customers' as name, 'green' as color, 'person-stalker' as icon, 'customer' as url FROM `".TABLE_CUSTOMER."`
                    UNION ALL
                    SELECT COUNT(*) as total, 'Visits' as name, 'aqua' as color, 'eye' as icon, 'visit' as url FROM `".TABLE_VISIT."` ". $whereCondition ."
                    UNION ALL
                    SELECT COUNT(*) as total, 'Meeting' as name, 'yellow' as color, 'clock' as icon, 'meeting' as url FROM `".TABLE_MEETING."` ". $whereCondition ."
                    UNION ALL
                    SELECT COUNT(*) as total, 'Closing' as name, 'red' as color, 'checkmark' as icon, 'closing' as url FROM `".TABLE_CLOSING."` ". $whereCondition;
            $countQry = $db -> query($countUnion);
            if (mysql_num_rows($countQry) > 0) {
                while ($countRow = mysql_fetch_array($countQry)) {
                    ?>
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-<?= $countRow['color']; ?>">
                            <div class="inner">
                                <h3><?= $countRow['total']; ?></h3>
                                <p><?= $countRow['name']; ?></p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-<?= $countRow['icon']; ?>"></i>
                            </div>
                            <a href="../<?= $countRow['url']; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reminders</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered no-margin">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Customer</th>
                                    <th>Reminder</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $remQry = $db -> query("SELECT r.date, r.description, c.name FROM `". TABLE_REMINDER ."` r INNER JOIN `". TABLE_CUSTOMER ."` c ON r.customer_id = c.id WHERE r.employee_id = '". $_SESSION['auth']['employee_id'] ."' AND r.date BETWEEN '". date('Y-m-d') ."' AND '".date('Y-m-d', strtotime("+3 days"))."' ORDER BY r.date ASC");
                                if (mysql_num_rows($remQry) > 0) {
                                    $i = 1;
                                    while ($reminder = mysql_fetch_array($remQry)) {
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?= $App -> dbformat_date_db_with_hyphen($reminder['date']); ?></td>
                                            <td><?= $reminder['name']; ?></td>
                                            <td><?= $reminder['description']; ?></td>
                                        </tr>
                                        <?php
                                        $i += 1;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="4">No reminders found.</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="../reminder" class="btn btn-primary pull-right">View All</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $db -> close();
        ?>
    </section>
<?php
require ("../footer.php");