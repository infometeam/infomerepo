<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers
            <!--<small>Control panel</small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customers</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        if (isset($_REQUEST['search']) && $_REQUEST['search']!= "") {
            $condition = "name LIKE '%".$_REQUEST['search']."%'";
        } else {
            $condition = 1;
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header clearfix">
                        <h3 class="box-title">List of Customers</h3>
                        <div class="table_filter_wrapper clearfix">
                            <form class="table_filters clearfix" method="get">
                                <input class="form-control" name="search" placeholder="Search" value="<?= (isset($_REQUEST['search']) && $_REQUEST['search']!= "") ? $_REQUEST['search'] : "" ?>" type="text">
                                <button class="btn btn-flat btn-success" type="submit"><i class="ion ion-search"></i></button>
                            </form>
                            <span class="pull-left"><a href="add.php" class="btn btn-primary btn-flat">Add New</a></span>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company</th>
                                <th>Level</th>
                                <th>Contact Person</th>
                                <th>Designation</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Fax</th>
                                <th>Po Box No</th>
                                <th>Description</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                            $db -> connect();
                            $qry = $db -> query("SELECT * FROM `". TABLE_CUSTOMER ."` WHERE ".$condition);
                            if (mysql_num_rows($qry) > 0) {
                                $i = 1;
                                while ($row = mysql_fetch_array($qry)) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $row['name']; ?></td>
                                        <td><?= $row['level']; ?></td>
                                        <td><?= $row['contact_person']; ?></td>
                                        <td><?= $row['designation']; ?></td>
                                        <td><a href="tel:<?= $row['mobile']; ?>"><?= $row['mobile']; ?></a></td>
                                        <td><a href="mailto:<?= $row['email']; ?>"><?= $row['email']; ?></a></td>
                                        <td><a href="tel:<?= $row['telephone']; ?>"><?= $row['telephone']; ?></a></td>
                                        <td><?= $row['fax']; ?></td>
                                        <td><?= $row['po_box_no']; ?></td>
                                        <td><?= $row['description']; ?></td>
                                        <td><?= $row['remarks']; ?></td>
                                        <td>
                                            <a href="edit.php?customer=<?= $row['id']; ?>" class="btn btn-flat btn-primary">Edit</a>
                                            <!--<button data-role="trigger_form" data-behaviour="confirm" data-verify="Are you sure you want to delete the customer?" class="btn btn-flat btn-danger">Delete
                                                <form class="form_hidden" method="post" action="do.php">
                                                    <input type="hidden" name="op" value="delete">
                                                    <input type="hidden" name="customer" value="<?/*= $row['id']; */?>">
                                                </form>
                                            </button>-->
                                        </td>
                                    </tr>
                                    <?php
                                    $i += 1;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">No data found</td>
                                </tr>
                                <?php
                            }
                            $db -> close();
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");