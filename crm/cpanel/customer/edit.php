<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customers</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }

        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Customer Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
                        $db -> connect();
                        $customerId = mysql_real_escape_string($_REQUEST['customer']);
                        $qry = $db -> query("SELECT * FROM `". TABLE_CUSTOMER ."` WHERE id = '". $customerId ."'");
                        $row = mysql_fetch_array($qry);
                        ?>
                        <form action="do.php?op=edit" method="post">
                            <input type="hidden" name="customer" value="<?= $customerId; ?>">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Company</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="name" class="form-control" name="name" value="<?= $row['name']; ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="level">Level</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <select class="form-control" name="level">
                                            <option value="1" <?= ($row['level'] == 1) ? "selected" : "" ?>>1</option>
                                            <option value="2" <?= ($row['level'] == 2) ? "selected" : "" ?>>2</option>
                                            <option value="3" <?= ($row['level'] == 3) ? "selected" : "" ?>>3</option>
                                            <option value="4" <?= ($row['level'] == 4) ? "selected" : "" ?>>4</option>
                                            <option value="5" <?= ($row['level'] == 5) ? "selected" : "" ?>>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact_person">Contact Person</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="contact_person" class="form-control" name="contact_person" value="<?= $row['contact_person'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="designation">Designation</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="designation" class="form-control" name="designation" value="<?= $row['designation'] ?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">Mobile</label>
                                        <input type="text" id="mobile" class="form-control" name="mobile" value="<?= $row['mobile'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="email" id="email" class="form-control" name="email" value="<?= $row['email'] ?>"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="telephone">Telephone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="telephone" class="form-control" name="telephone" value="<?= $row['telephone'] ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="fax">Fax</label>
                                        <input type="text" id="fax" class="form-control" name="fax" value="<?= $row['fax'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label for="po_box_no">Post Box No</label>
                                        <input type="text" id="po_box_no" class="form-control" name="po_box_no" value="<?= $row['po_box_no'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"><?= $row['description'] ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea name="remarks" id="remarks" class="form-control"><?= $row['remarks'] ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        $db -> close();
                        ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");