<?php
require("../header.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= $dashboardUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Customers</li>
        </ol>
    </section>
    <section class="content">
        <?php
        if (isset($_SESSION['flash'])) {
            echo $_SESSION['flash'];
            unset($_SESSION['flash']);
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Customer</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="do.php?op=new" method="post">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Company</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="name" class="form-control" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="level">Level</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <select class="form-control" name="level">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact_person">Contact Person</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="contact_person" class="form-control" name="contact_person" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="designation">Designation</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="designation" class="form-control" name="designation" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile">Mobile</label>
                                        <input type="text" id="mobile" class="form-control" name="mobile">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="email" id="email" class="form-control" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="telephone">Telephone</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="text" id="telephone" class="form-control" name="telephone" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="fax">Fax</label>
                                        <input type="text" id="fax" class="form-control" name="fax">
                                    </div>
                                    <div class="form-group">
                                        <label for="po_box_no">Post Box No</label>
                                        <input type="text" id="po_box_no" class="form-control" name="po_box_no">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="remarks">Remarks</label>
                                        <textarea name="remarks" id="remarks" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
<?php
require ("../footer.php");