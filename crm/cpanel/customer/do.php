<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if (!$_SESSION['auth'] || !$_SESSION['auth']['id']) {
    header("location: ../../logout.php");
}
$op = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch ($op) {
    case "new" :
        if (!$_POST['name'] || !$_POST['level'] || !$_POST['contact_person'] || !$_POST['designation'] || !$_POST['telephone'] || !$_POST['email']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $email = $App -> convert($_POST['email']);
            $existence = $db -> checkValueExist(TABLE_CUSTOMER, "email = '". $email ."'");
            if ($existence) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Error! Email Id already exists.");
                header("location: add.php");
            } else {
                $customer = [
                    "name" => $App -> convert($_POST['name']),
                    "level" => $App -> convert($_POST['level']),
                    "contact_person" => $App -> convert($_POST['contact_person']),
                    "designation" => $App -> convert($_POST['designation']),
                    "mobile" => $App -> convert($_POST['mobile']),
                    "email" => $App -> convert($email),
                    "telephone" => $App -> convert($_POST['telephone']),
                    "fax" => $App -> convert($_POST['fax']),
                    "po_box_no" => $App -> convert($_POST['po_box_no']),
                    "description" => $App -> convert($_POST['description']),
                    "remarks" => $App -> convert($_POST['remarks']),
                    "log_id" => $_SESSION['auth']['id']
                ];
                $status = $db -> query_insert(TABLE_CUSTOMER, $customer);
                if ($status) {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(1, "Successfully added customer.");
                    header("location: ../customer");
                } else {
                    $db -> close();
                    $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                    header("location: add.php");
                }
            }
        }
        break;
    case "edit" :
        if (!$_POST['name'] || !$_POST['level'] || !$_POST['contact_person'] || !$_POST['designation'] || !$_POST['telephone'] || !$_POST['email']) {
            $_SESSION['flash'] = $App->session_flash(0, "Error! Invalid details.");
            header("location: add.php");
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $customerId = $_POST['customer'];
            $customer = [
                "name" => $App -> convert($_POST['name']),
                "level" => $App -> convert($_POST['level']),
                "contact_person" => $App -> convert($_POST['contact_person']),
                "designation" => $App -> convert($_POST['designation']),
                "mobile" => $App -> convert($_POST['mobile']),
                "email" => $App -> convert($App -> convert($_POST['email'])),
                "telephone" => $App -> convert($_POST['telephone']),
                "fax" => $App -> convert($_POST['fax']),
                "po_box_no" => $App -> convert($_POST['po_box_no']),
                "description" => $App -> convert($_POST['description']),
                "remarks" => $App -> convert($_POST['remarks'])
            ];
            $status = $db -> query_update(TABLE_CUSTOMER, $customer, "id = '". $customerId ."'");
            if ($status) {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(1, "Successfully updated customer details.");
                header("location: edit.php?customer=".$customerId);
            } else {
                $db -> close();
                $_SESSION['flash'] = $App->session_flash(0, "Something went wrong. Please try again.");
                header("location: edit.php?customer=".$customerId);
            }
            $db -> close();
        }
        break;
    case "live" :
        header('Content-type:application/json');
        $result = [
            "status" => 1
        ];
        if (!$_POST['name'] || !$_POST['level'] || !$_POST['contact_person'] || !$_POST['designation'] || !$_POST['telephone'] || !$_POST['email']) {
            $result["status"] = 0;
            $result["error"] = "Error! Invalid details";
        } else {
            $db	 = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db -> connect();
            $email = $App -> convert($_POST['email']);
            $existence = $db -> checkValueExist(TABLE_CUSTOMER, "email = '". $email ."'");
            if ($existence) {
                $db -> close();
                $result["status"] = 0;
                $result["error"] = "Error! Email Id already exists.";
            } else {
                $customer = [
                    "name" => $App -> convert($_POST['name']),
                    "level" => $App -> convert($_POST['level']),
                    "contact_person" => $App -> convert($_POST['contact_person']),
                    "designation" => $App -> convert($_POST['designation']),
                    "mobile" => $App -> convert($_POST['mobile']),
                    "email" => $App -> convert($email),
                    "telephone" => $App -> convert($_POST['telephone']),
                    "fax" => $App -> convert($_POST['fax']),
                    "po_box_no" => $App -> convert($_POST['po_box_no']),
                    "description" => $App -> convert($_POST['description']),
                    "remarks" => $App -> convert($_POST['remarks']),
                    "log_id" => $_SESSION['auth']['id']
                ];
                $status = $db -> query_insert(TABLE_CUSTOMER, $customer);
                if ($status) {
                    $db -> close();
                    $result["customer_id"] = $status;
                } else {
                    $db -> close();
                    $result["status"] = 0;
                    $result["error"] = "Something went wrong. Please try again.";
                }
            }
        }
        echo json_encode($result);
        break;
    case "delete" :
        echo "delete";
        break;
}