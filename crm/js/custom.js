$(function () {
    // Show file upload thumbnail before submitting the form
    /*function showThumbnail(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $(input).nextAll('.image_upload_preview').remove();
            reader.onload = function (e) {
                $(input).after('<img class="image_upload_preview" src="' + e.target.result + '" alt=""/>');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on('change', 'input[type="file"][data-action="show_thumbnail"]', function () {
        showThumbnail($(this).get(0));
    });*/

    // Hiding session message after a few seconds
    setTimeout(function () {
        $('div[data-role="auto-hide"]').fadeOut();
    }, 5000);
    $(document).on('click', 'div[data-role="auto-hide"]', function () {
        $(this).fadeOut();
    });

    // Triggering submit the hidden forms
    $(document).on('click', 'button[data-role="trigger_form"]', function(e) {
        e.preventDefault();
        var trigger = $(this);
        if (trigger.attr('data-behaviour') == "confirm") {
            var verify = trigger.attr('data-verify'),
                flag = confirm(verify);
            if (flag) {
                trigger.find('form.form_hidden').submit();
            }
        } else {
            trigger.find('form.form_hidden').submit();
        }
    });
    if ($('.date-picker').length) {
        $('.date-picker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }
    // Live search for customer
    $(document).on('keyup focus input', 'input[data-role="trigger_customer_search"]', function () {
        var searchBox = $(this),
            searchKey = searchBox.val(),
            liveList = searchBox.nextAll('ul.live_list');
        if (searchKey) {
            $.ajax({
                method: 'POST',
                url: '../../cpanel/services/customer.php',
                data: {
                    searchKey: searchKey
                },
                success: function (result) {
                    if (result.status && result.count > 0) {
                        liveList.empty();
                        $.each(result.customers, function (k, v) {
                            liveList.append('<li data-customer_id="'+ v.id +'" data-customer="' + v.name + '">'+ v.name +'<div class="tool_tip"><span></span>'+ v.contact_person +'</div></li>');
                        });
                    } else {
                        liveList.empty();
                    }
                },
                error: function (error) {
                    liveList.empty();
                }
            });
        } else {
            liveList.empty();
        }
    });
    $(document).on('focusout', 'input[data-role="trigger_customer_search"]', function () {
        $(this).siblings('ul.live_list').delay(300).fadeOut();
    });
    $(document).on('focus', 'input[data-role="trigger_customer_search"]', function () {
        $(this).siblings('ul.live_list').show();
    });
    $(document).on('click', 'ul.live_list > li', function () {
        var li = $(this),
            customer = li.attr('data-customer'),
            customer_id = li.attr('data-customer_id');
        li.closest('.form-group').find('input[name="customer"]').val(li.attr('data-customer'));
        li.closest('.form-group').find('input[name="customer_id"]').val(li.attr('data-customer_id'));
        li.closest('ul').hide();
    });

    // iCheck
    $('input[type="checkbox"][data-role="trigger_dynamic_form"]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '15%'
    });

    // Dynamic form
    $('input[type="checkbox"][data-role="trigger_dynamic_form"]').on('ifChecked', function () {
        var source = $($(this).attr('data-source')).html();
        console.log(source);
        $(this).closest('form').find('.dynamic_elements').append(source);
        $('.dynamic_datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    });
    $('input[type="checkbox"][data-role="trigger_dynamic_form"]').on('ifUnchecked', function () {
        $(this).closest('form').find('.dynamic_elements').empty();
    });

    // Live form for adding customer
    $(document).on('click', '#trigger_customer_live', function () {
        var btn = $(this),
            form = $('form.customer_live_form');
        if (form[0].checkValidity()) {
            $('.loader_overlay').fadeIn(100);
            var customer = {
                op: "live",
                name: form.find('input[name="name"]').val(),
                level: form.find('select[name="level"]').val(),
                contact_person: form.find('input[name="contact_person"]').val(),
                designation: form.find('input[name="designation"]').val(),
                mobile: form.find('input[name="mobile"]').val(),
                email: form.find('input[name="email"]').val(),
                telephone: form.find('input[name="telephone"]').val(),
                fax: form.find('input[name="fax"]').val(),
                po_box_no: form.find('input[name="po_box_no"]').val(),
                description: form.find('textarea[name="description"]').val(),
                remarks: form.find('textarea[name="remarks"]').val()
            };
            $.ajax({
                method: 'POST',
                url: '../customer/do.php',
                data: customer,
                success: function (result) {
                    if (result.status == 1) {
                        $('form[data-listen="customer_live"]').find('input[name="customer"]').val(customer.name);
                        $('form[data-listen="customer_live"]').find('input[name="customer_id"]').val(result.customer_id);
                        $('.loader_overlay').fadeOut(100);
                        btn.closest('.modal').modal('hide');
                    } else {
                        $('.loader_overlay').fadeOut(100);
                        alert("Something went wrong. Please try again.");
                    }
                },
                error: function (error) {
                    console.log(error);
                    $('.loader_overlay').fadeOut(100);
                    alert("Something went wrong. Please try again.");
                }
            });
        } else {
            alert("Error! Invalid details.");
        }
    });
});