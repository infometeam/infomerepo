<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Infome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Signika:400,600" rel="stylesheet">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/pratical.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/menu.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/ddmenu.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/style.css">
    <link rel="stylesheet" href="<?= bloginfo("stylesheet_url"); ?>">
    <?php wp_head(); ?>
</head>
<body>
<header>
    <!-- <header id="to-affix" class="navbar-inverse" data-spy="affix" data-offset-top="0">-->
    <!--<div class="nav_bar navbar navbar-inverse" id="to-affix">-->
    <div class="container">
        <div class="nav_bar">
            <div class="logo">
                <a href="<?= site_url(); ?>"><img src="<?= get_template_directory_uri(); ?>/images/logo.png" alt="Infome UAE"></a>
            </div>
            <div class="top_contact"><a href="tel:+97143546020"><span><i class="fa fa-phone" aria-hidden="true"></i>+97143546020</span></a>
                <a href="mailto:sales@infomeuae.com"><span><i class="fa fa-envelope-o" aria-hidden="true"></i>sales@infomeuae.com</span></a>
            </div>
            <nav class="main_nav" id="ddmenu">
                <ul>
                    <li><a href="<?= site_url(); ?>">Home</a></li>
                    <li><a href="<?= get_permalink(141); ?>">What we do</a></li>
                    <li><a href="<?= get_permalink(138); ?>">Who we are</a></li>
                    <li class="full-width"><a href="#">Products
                            <i class="caret"></i></a>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <div class="row">

                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(11); ?>"><h3>Server &amp; Storage</h3></a></li>
                                        <?php
                                        $serverPages = get_sub_pages_of(11);
                                        foreach ($serverPages as $serverPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($serverPage -> ID); ?>"><?= $serverPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(74); ?>"><h3>Desktops</h3></a></li>
                                        <?php
                                        $desktopPages = get_sub_pages_of(74);
                                        foreach ($desktopPages as $desktopPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($desktopPage -> ID); ?>"><?= $desktopPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(98); ?>"><h3>Laptops</h3></a></li>
                                        <?php
                                        $laptopPages = get_sub_pages_of(98);
                                        foreach ($laptopPages as $laptopPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($laptopPage -> ID); ?>"><?= $laptopPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-4">
                                        <li><a href="<?= get_permalink(175); ?>"><h3>ID card printers</h3></a></li>
                                        <?php
                                        $idPages = get_sub_pages_of(175);
                                        foreach ($idPages as $idPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($idPage -> ID); ?>"><?= $idPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(147); ?>"><h3>Network Products</h3></a></li>
                                        <?php
                                        $networkCats = get_field('product_categories', 147);
                                        foreach ($networkCats as $networkCat) {
                                            ?>
                                            <li><a href="<?= get_permalink(147); ?>"><?= $networkCat -> name; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <hr>
                                <div class="row">
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(597); ?>"><h3>Pabx &amp; Office Telephone</h3></a></li>
                                        <?php
                                        $pabProPages = get_sub_pages_of(597);
                                        foreach ($pabProPages as $pabProPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($pabProPage -> ID); ?>"><?= $pabProPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-4">
                                        <li><a href="<?= get_permalink(155); ?>"><h3>Point of Sale</h3></a></li>
                                        <?php
                                        $pointPages = get_sub_pages_of(155);
                                        foreach ($pointPages as $pointPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($pointPage -> ID); ?>"><?= $pointPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(123); ?>"><h3>Printers and Scanners</h3></a></li>
                                        <?php
                                        $printPages = get_sub_pages_of(123);
                                        foreach ($printPages as $printPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($printPage -> ID); ?>"><?= $printPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-4">
                                        <li><a href="<?= get_permalink(151); ?>"><h3>Software Products</h3></a></li>
                                        <?php
                                        $softCats = get_field('product_categories', 151);
                                        foreach ($softCats as $softCat) {
                                            ?>
                                            <li><a href="<?= get_permalink(151); ?>"><?= $softCat -> name; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="full-width solutions_menu">
                        <a href="#">Solutions <i class="caret"></i></a>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <div class="row">
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(619); ?>"><h3>Attendance &amp; access control</h3></a></li>
                                        <li><a href="<?= get_permalink(621); ?>"><h3>CCTV surveillance</h3></a></li>
                                        <li><a href="<?= get_permalink(617); ?>"><h3>ID card personalisation</h3></a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(612); ?>"><h3>Network Security</h3></a></li>
                                        <?php
                                        $networkPages = get_sub_pages_of(612);
                                        foreach ($networkPages as $networkPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($networkPage -> ID); ?>"><?= $networkPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                        <li><a href="<?= get_permalink(610); ?>"><h3>Pabx &amp; Telephones</h3></a></li>
                                        <?php
                                        $pabPages = get_sub_pages_of(610);
                                        foreach ($pabPages as $pabPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($pabPage -> ID); ?>"><?= $pabPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                        <li><a href="<?= get_permalink(606); ?>"><h3>Server Solutions</h3></a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(608); ?>"><h3>Storage Solutions</h3></a></li>
                                        <?php
                                        $storagePages = get_sub_pages_of(608);
                                        foreach ($storagePages as $storagePage) {
                                            ?>
                                            <li><a href="<?= get_permalink($storagePage -> ID); ?>"><?= $storagePage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                        <li><a href="<?= get_permalink(637); ?>"><h3>Structured Cabling</h3></a></li>
                                        <li><a href="<?= get_permalink(615); ?>"><h3>Wifi Solutions</h3></a></li>
                                        <?php
                                        $wifiPages = get_sub_pages_of(615);
                                        foreach ($wifiPages as $wifiPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($wifiPage -> ID); ?>"><?= $wifiPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="<?= get_permalink(662); ?>"><h3>Infome Lab service</h3></a></li>
                                        <?php
                                        $infPages = get_sub_pages_of(662);
                                        foreach ($infPages as $infPage) {
                                            ?>
                                            <li><a href="<?= get_permalink($infPage -> ID); ?>"><?= $infPage -> post_title; ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--<li><a href="<?/*= site_url(); */?>/blog">Blog</a></li>-->
                    <li><a href="<?= get_permalink(131); ?>">Contact us</a></li>
                </ul>
            </nav>
            <div class="open mobile">
                <span class="cls"></span>
                <span>
                    <ul class="sub-menu ">
                        <li>
                            <a href="<?= site_url(); ?>" title="Home">Home</a>
                        </li>
                        <li>
                            <a href="<?= get_permalink(141); ?>" title="What we do">What we do</a>
                        </li>
                        <li>
                            <a href="<?= get_permalink(138); ?>" title="Who We Are">Who We Are</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Products"><h3>Products</h3></a>
                            <ul class="mobile_sub">
                                <li><a href="<?= get_permalink(11); ?>">Server &amp; Storage</a></li>
                                <li><a href="<?= get_permalink(74); ?>">Desktops</a></li>
                                <li><a href="<?= get_permalink(98); ?>">Laptops</a></li>
                                <li><a href="<?= get_permalink(175); ?>">ID card printers</a></li>
                                <li><a href="<?= get_permalink(147); ?>">Network Products</a></li>
                                <li><a href="<?= get_permalink(597); ?>">Pabx &amp; Office Telephone</a></li>
                                <li><a href="<?= get_permalink(155); ?>">Point of Sale</a></li>
                                <li><a href="<?= get_permalink(123); ?>">Printers and Scanners</a></li>
                                <li><a href="<?= get_permalink(151); ?>">Software Products</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="Solutions"><h3>Solutions</h3></a>
                            <ul class="mobile_sub">
                                <li><a href="<?= get_permalink(619); ?>">Attendance &amp; access control</a></li>
                                <li><a href="<?= get_permalink(621); ?>">CCTV surveillance</a></li>
                                <li><a href="<?= get_permalink(617); ?>">ID card personalisation</a></li>
                                <li><a href="<?= get_permalink(612); ?>">Network Security</a></li>
                                <li><a href="<?= get_permalink(610); ?>">Pabx &amp; Telephones</a></li>
                                <li><a href="<?= get_permalink(606); ?>">Server Solutions</a></li>
                                <li><a href="<?= get_permalink(637); ?>">Structured Cabling</a></li>
                                <li><a href="<?= get_permalink(615); ?>">Wifi Solutions</a></li>
                                <li><a href="<?= get_permalink(662); ?>">Infome Lab service</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="<?/*= site_url(); */?>/blog" title="Blog">Blog</a></li>-->
                        <li><a href="<?= get_permalink(131); ?>" title="Contact Us">Contact us</a></li>
                    </ul>
                </span>
                <span class="cls"></span>
            </div>
        </div>
    </div>
</header>