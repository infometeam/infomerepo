<?php
$currentPageId = get_the_ID();
$hmPage = get_post($currentPageId);
?>
<div class="feed_back">
    <span class="icon spn"><img src="<?= get_template_directory_uri(); ?>/images/feed_back_icon.png" alt="Infome"></span>
    <span class="close spn"><img src="<?= get_template_directory_uri(); ?>/images/close.png" alt="Infome"></span>
    <div class='wrap'>
        <div class='content'>
            <?= do_shortcode('[contact-form-7 id="255" title="Quick Contact"]'); ?>
        </div>
    </div>
</div>
<div class="banner hm_banner" id="main">
    <div id="particles-js">
        <div class="container">
            <div class="caption">
                <h3>In Touch</h3>
                <h1>with Tomorrow</h1>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-theme" id="owl-demo1">
    	<div class="owl-item"><img src="<?= get_template_directory_uri(); ?>/images/ban_a1.jpg" alt=""></div>
    	<div class="owl-item"><img src="<?= get_template_directory_uri(); ?>/images/ban_a2.jpg" alt=""></div>
    	<div class="owl-item"><img src="<?= get_template_directory_uri(); ?>/images/ban_a3.jpg" alt=""></div>
    	<div class="owl-item"><img src="<?= get_template_directory_uri(); ?>/images/ban_a4.jpg" alt=""></div>
    </div>
</div>
<div class="who_we_are" id="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="who_we">
                    <h1>WHO WE ARE</h1>
                    <p><?= $hmPage -> post_content; ?></p>
                    <a href="<?= site_url(); ?>/who-we-are">Know more</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="make_deffrents">
                    <h1>HOW WE MAKE A DIFFERENCE</h1>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="icon_box">
                                <img src="<?= get_template_directory_uri(); ?>/images/fast.png" alt="Infome Quick response">
                                <h4>Quick response</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="icon_box">
                                <img src="<?= get_template_directory_uri(); ?>/images/price.png" alt="Infome Quick response">
                                <h4>Best price in the market</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="icon_box">
                                <img src="<?= get_template_directory_uri(); ?>/images/support.png" alt="Infome Quick response">
                                <h4>Professional and quick
                                    technical support</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="products">
    <div class="container">
        <h1>PRODUCTS</h1>
        <span><img src="<?= get_template_directory_uri(); ?>/images/row.png" alt="Infome"></span>
        <div class="product_slider">
            <div id="demo">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <div id="owl-demo" class="owl-carousel">
                                <?php
                                $products = get_posts (array(
                                    "numberposts" => 9,
                                    "post_type" => "product",
                                    "cat" => 67
                                ));
                                foreach ($products as $product) {
                                    ?>
                                    <div class="item">
                                        <a class="product_box" href="<?= get_permalink($product -> ID); ?>">
                                            <img src="<?= get_post_image_url_with_default($product -> ID, 'full', true); ?>" alt="<?= $product -> post_title; ?>">
                                            <h4><?= $product -> post_title; ?></h4>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="customNavigation">
                                <div class="prev"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></div>
                                <div class="next"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="solutions hm_solutions">
    <div class="container">
        <h1>SOLUTIONS</h1>
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-md-4">
                <ul>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(619); ?>">Attendance and access control</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(621); ?>">CCTV surveillance</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(617); ?>">ID card personalisation</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(612); ?>">Network Security</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
                <ul>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(610); ?>">Pabx and Telephones</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(606); ?>">Server Solutions</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(608); ?>">Storage Solutions</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(637); ?>">Structured cabling</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
                <ul>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(615); ?>">Wifi Solutions</a></li>
                    <li><span><img src="<?= get_template_directory_uri(); ?>/images/solution_icon.png" alt="Infome Solutions"></span><a href="<?= get_permalink(662); ?>">Infome Lab service</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="dealing_with">
    <div class="container">
        <div id="demo">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div id="owl-demo2" class="owl-carousel">
                            <?php
                            $brands = get_posts(array(
                                "numberposts" => 9,
                                "cat" => 66
                            ));
                            foreach ($brands as $brand) {
                                ?>
                                <div class="item">
                                    <img src="<?= get_post_image_url_with_default($brand -> ID, "full", true) ?>" alt="<?= $brand -> post_title; ?>">
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>