<?php
get_header();
$current_post_id = get_the_ID();
$current_post = get_page($current_post_id);
$featured_image = get_post_image_url_with_default($current_post_id, 'full', true);
$postUrl = get_permalink();
?>
<div class="inner inner_secnd" id="main">
    <!--<div class="heading_tab">
        <div class="container">
            <span><img src="images/HP_logo_630x630.png" alt="HP"></span><h1>HP Servers</h1>
        </div>
    </div>-->
    <div class="product_details">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h1><?= $current_post -> post_title; ?></h1>
                <img class="img-responsive" src="<?= $featured_image; ?>" alt="<?= $current_post -> post_title; ?>">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="detail_of_product">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu1">OVERVIEW</a></li>
                        <li><a data-toggle="tab" href="#menu2">SPECIFICATIONS</a></li>
                        <li><a data-toggle="tab" href="#menu3">ENQUIRE NOW</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="menu1" class="tab-pane fade in active" style="padding-top:30px;">
                            <div><?= $current_post -> post_content; ?></div>
                            <?php
                            $brochure = get_field("brochure");
                            if ($brochure != false) {
                                ?>
                                <div class="brochure_lnk"><a href="<?= $brochure ?>"><i class="fa fa-download"></i>Download Brochure</a></div>
                                <?php
                            }
                            ?>
                        </div>
                        <div id="menu2" class="tab-pane fade" style="padding-top:30px;">
                            <div><?= get_field('product_specifications', $current_post_id); ?></div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="solution_form_wrapper">
                                <div class="solution_form">
                                    <?= do_shortcode('[contact-form-7 id="238" title="Product Enquiry"]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bottom_row"></div>
<?php get_footer(); ?>