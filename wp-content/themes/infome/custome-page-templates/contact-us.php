<?php
/**
 * Template Name: Contact Us
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
<div class="inner inner_secnd" id="main">
    <div class="container contact_wrapper">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="contact_head"><h3>Send Us a Message</h3><span></span></div>
            <div class="contact_content"><?= $currentPage -> post_content; ?></div>
            <div class="contact_form">
                <?= do_shortcode('[contact-form-7 id="130" title="Contact Us"]'); ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="contact_aside">
                <div class="contact_image"><img src="<?= get_post_image_url_with_default($currentPageId, "full", true); ?>" class="img-responsive"></div>
                <h3>Our Address</h3>
                <p>DUBAI, UAE</p>
                <div class="contact_row"><i class="fa fa-envelope"></i> <a href="mailto:info@infomeuae.com">info@infomeuae.com</a></div>
                <div class="contact_row"><i class="fa fa-phone"></i> <a href="tel:+97143546020">+971 4 354 6020</a></div>
                <div class="contact_row"><i class="fa fa-fax"></i> +971 4 354 6028</div>
            </div>
        </div>
    </div>
</div>
<div id="gmap"></div>

<script>
    function initMap() {
        // Styles a map in night mode.
        var myLatLng = {lat: 25.261024, lng: 55.286432},
            contentString = '<div id="content"><h4>Infome UAE</h4></div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 250
        });
        var map = new google.maps.Map(document.getElementById('gmap'), {
            center: myLatLng,
            zoom: 18,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Bodhi Info Solutions Pvt Ltd',
            animation: google.maps.Animation.BOUNCE
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
        map.addListener('idle', function () {
            infowindow.open(map, marker);
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj335zTM9brgk0tmx4Fl5CGTeLItgIZbI&callback=initMap" sync defer></script>
<?php get_footer(); ?>
