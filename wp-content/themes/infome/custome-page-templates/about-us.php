<?php
/**
 * Template Name: About Us
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
<div class="inner inner_secnd about_wrap" id="main">
    <div class="container">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="about_head">
                <h1 class="text-uppercase">who we are</h1>
                <span class="about_head_border"></span>
            </div>
            <h4 class="about_caption">We Provide the Best Solution to our Customer with trust and We are happy</h4>
            <div><?= $currentPage -> post_content; ?></div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <img class="img-responsive" src="<?= get_post_image_url_with_default($currentPageId, "full", true); ?>" alt="">
        </div>
    </div>
</div>
<div class="about_counter_wrapper">
    <div class="about_counter_inner">
        <div class="container text-center">
            <div class="counter">
                <div class="counter_num">100%</div>
                <h4>Success Rate</h4>
            </div>
            <div class="counter">
                <div class="counter_num">200+</div>
                <h4>Happy Clients</h4>
            </div>
            <div class="counter">
                <div class="counter_num">100%</div>
                <h4>Success Rate</h4>
            </div>
            <div class="counter">
                <div class="counter_num">100%</div>
                <h4>Success Rate</h4>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>