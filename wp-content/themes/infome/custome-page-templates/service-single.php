<?php
/**
 * Template Name: Solution Category Page
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
<div class="inner inner_secnd" id="main">
    <div class="container service_header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5">
                <img class="img-responsive" src="<?= get_post_image_url_with_default($currentPageId, "full", true) ?>">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7">
                <h1 class="service_heading"><?= $currentPage -> post_title; ?></h1>
                <div><?= $currentPage -> post_content; ?></div>
            </div>
        </div>
    </div>
    <div class="inner_cat">
        <div class="container">
            <div class="row">
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $solutionCategory = get_field("solution_category", $currentPageId);
                $solutions = query_posts(array(
                    "posts_per_page" => 6,
                    "category__in" => $solutionCategory,
                    "post_type" => "solution",
                    "paged" => $paged
                ));
                $i = 1;
                foreach ($solutions as $solution) {
                $url = get_permalink($solution -> ID);
                $imageUrl = get_post_image_url_with_default($solution -> ID, 'thumbnail', true);
                ?>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="service_box">
                        <div class="service_box_img">
                            <img src="<?= $imageUrl; ?>" alt="">
                        </div>
                        <h2 class="service_box_title"><a href="<?= $url; ?>"><?= $solution -> post_title; ?></a></h2>
                        <div class="service_box_lnk">
                            <a href="<?= $url; ?>">Read More</a>
                        </div>
                    </div>
                </div>
                <?php
                if ($i == 3) {
                ?>
            </div>
            <div class="row">
                <?php
                $i = 0;
                }
                $i += 1;
                }
                ?>
            </div>
            <nav class="pagination">
                <?php pagination_bar(); ?>
            </nav>
        </div>
    </div>
</div>
<div class="bottom_row"></div>
<?php get_footer(); ?>
