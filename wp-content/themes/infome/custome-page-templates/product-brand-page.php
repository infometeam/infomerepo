<?php
/**
 * Template Name: Product Brand Page
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
<div class="inner inner_secnd" id="main">
    <div class="heading_tab">
        <div class="container">
            <span><img src="<?= get_post_image_url_with_default($currentPageId, 'full', true); ?>" alt="<?= $currentPage -> post_title; ?>"></span><h1><?= $currentPage -> post_title; ?></h1>
        </div>
    </div>
    <div class="banner">
        <div class="container">
            <div id="owl-demo1" class="owl-carousel owl-theme">
                <?php
                if( class_exists('Dynamic_Featured_Image') ) {
                    global $dynamic_featured_image;
                    $featured_images = $dynamic_featured_image->get_featured_images( $currentPageId );
                }
                foreach ($featured_images as $featured_image) {
                    ?>
                    <div class="item"><img src="<?= $featured_image['full']; ?>" alt=""></div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="inner_about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="content">
                        <h1><?= $currentPage -> post_title; ?></h1>
                        <div>
                            <?= $currentPage -> post_content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="inner_cat">
        <div class="container">
            <div class="row">
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $brandCategory = get_field("brand_category", $currentPageId);
                $branProducts = query_posts(array(
                    "posts_per_page" => 8,
                    "cat" => $brandCategory -> term_taxonomy_id,
                    "post_type" => "product",
                    "paged" => $paged
                ));
                if (count($branProducts) > 0) {
                    $i = 1;
                    foreach ($branProducts as $branProduct) {
                    $url = get_permalink($branProduct -> ID);
                    $imageUrl = get_post_image_url_with_default($branProduct -> ID, 'thumbnail', true);
                    ?>
                    <div class="col-lg-3 col-sm-3 col-md-3">
                        <div class="inner_cat_bx">
                            <a href="<?= $url; ?>"><img src="<?= $imageUrl; ?>" alt="<?= $branProduct -> post_title; ?>">
                                <div class="overlay">
                                    <span><i class="fa fa-expand" aria-hidden="true"></i></span>                                    
                                </div>
                                <h2 class="text-center"><?= $branProduct -> post_title; ?></h2>
                            </a>
                        </div>
                    </div>
                    <?php
                        if ($i == 4) {
                        ?>
                            </div>
                            <div class="row">
                        <?php
                        $i = 0;
                        }
                    $i += 1;
                    }
                } else {
                    ?>
                    <div class="alert alert-danger">No products found under this category.</div>
                    <?php
                }
                ?>
            </div>
            <nav class="pagination">
                <?php pagination_bar(); ?>
            </nav>
        </div>
    </div>
</div>
<div class="bottom_row"></div>
<?php get_footer(); ?>
