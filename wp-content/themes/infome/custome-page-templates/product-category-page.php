<?php
/**
 * Template Name: Product Category Page
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
<div class="inner" id="main">
    <div class="heading_tab">
        <div class="container">
            <div class="head_row"><h1><?= $currentPage -> post_title; ?></h1></div>
        </div>
    </div>
    <?php
    $pages = get_pages(array(
        "parent" => $currentPageId
    ));
    ?>
    <div class="banner">
        <div class="container">
            <div id="owl-demo1" class="owl-carousel owl-theme">
                <?php
                foreach ($pages as $page) {
                    if( class_exists('Dynamic_Featured_Image') ) {
                        global $dynamic_featured_image;
                        $featured_images = $dynamic_featured_image->get_featured_images( $page ->ID );
                    }
                    ?>
                    <div class="item"><img src="<?= $featured_images[0]['full']; ?>" alt="<?= $page -> post_title; ?>"></div>
                    <?php
                }
                ?>
            </div>
            <!--<div class="customNavigation">
               <div class="prev"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></div>
               <div class="next"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></div>
            </div>-->
        </div>
    </div>

    <div class="inner_about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="content">
                        <h1><?= $currentPage -> post_title; ?></h1>
                        <div><?= $currentPage -> post_content; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="inner_cat">
        <div class="container">
            <div class="row">
                <?php
                $i = 1;
                foreach ($pages as $page) {
                    $pageUrl = get_permalink($page -> ID);
                    $imageUrl = get_post_image_url_with_default($page -> ID, 'full', true);
                    ?>
                    <div class="col-lg-3 col-sm-3 col-md-3">
                        <div class="inner_cat_bx">
                            <a href="<?= $pageUrl; ?>"><img src="<?= $imageUrl; ?>" alt="<?= $page -> post_title; ?>">
                                <div class="overlay">
                                    <span><i class="fa fa-expand" aria-hidden="true"></i></span>
                                </div>
                                <h2><?= $page -> post_title; ?></h2>
                            </a>
                        </div>
                    </div>
                    <?php
                    if ($i == 4) {
                        ?>
                        </div><div class="row">
                        <?php
                        $i = 0;
                    }
                $i += 1;
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="bottom_row"></div>


<?php get_footer(); ?>
