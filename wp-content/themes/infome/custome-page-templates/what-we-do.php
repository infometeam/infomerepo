<?php
/**
 * Template Name: What We Do
 */
$currentPageId = get_the_ID();
$currentPage = get_post($currentPageId);
get_header();
?>
    <div class="inner inner_secnd about_wrap" id="main">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="about_head">
                    <h1 class="text-uppercase">What we do</h1>
                    <span class="about_head_border"></span>
                </div>
                <h4 class="about_caption">We Provide the Best Solution to our Customer with trust and We are happy</h4>
                <div><?= $currentPage -> post_content; ?></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <img class="img-responsive" src="<?= get_post_image_url_with_default($currentPageId, "full", true); ?>" alt="">
            </div>
        </div>
    </div>
    <div class="what_we_do_wrapper">
        <div class="container">
            <div class="row">
                <?php
                $what_we_dos = get_posts(array(
                    "posts_per_page" => -1,
                    "cat" => 24
                ));
                $i = 1;
                foreach ($what_we_dos as $what_we_do) {
                    $url = get_permalink($what_we_do -> ID);
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="what_we_do_box">
                            <h3><a href="<?= $url ?>"><?= $what_we_do -> post_title; ?></a></h3>
                            <div class="what_we_do_content"><?= limit_words($what_we_do -> post_content, 10); ?></div>
                            <div class="read_more"><a href="<?= $url ?>">Read More</a></div>
                            <div class="what_we_do_border_1"></div>
                            <div class="what_we_do_border"></div>
                        </div>
                    </div>
                    <?php
                    if ($i == 3) {
                        ?>
                        </div><div class="row">
                        <?php
                        $i = 0;
                    }
                    $i += 1;
                }
                ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>