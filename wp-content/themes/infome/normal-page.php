<?php

$current_post_id = get_the_ID();

$current_post = get_page($current_post_id);

$featImg = get_post_image_url_with_default($current_post_id, 'full', false);

?>








<?php if (!is_null($featImg)) {

    echo '<div class="header-image"><img src="' . $featImg . '" alt="' . $current_post->post_title . '" /></div>';

} ?>
<div class="container">
    <h2 class="page_head text-center"><?= $current_post->post_title ?></h2>
    <div class="page_content">
        <?= apply_filters('the_content', $current_post->post_content); ?>
    </div>
</div>


<!--bottom content-->
