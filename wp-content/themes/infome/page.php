<?php get_header();
$page_type = current_page_type(get_the_ID()); ?>
    <!--content-->

<?php
switch ($page_type) {
    case "home" :
        include 'home-page.php';
        break;
    case "know-your-rights" :
        include 'pages/know-your-rights.php';
        break;
    case "contact" :
        include 'pages/contact.php';
        break;
    case "news-room" :
        include 'pages/news-room.php';
        break;
    case "press-release" :
        include 'pages/press-release.php';
        break;

    default:
        include 'normal-page.php';
        break;

}
?>

    <!--/content-->
<?php get_footer(); ?>