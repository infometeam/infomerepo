<?php get_header();
$cat_id = get_query_var('cat');
$category_details = get_category($cat_id);
$cat_name = $category_details->category_nicename;
?>
<div class="page_wrapper">
	<div class="container">
        <div class="cat_page_hd">
            <h3><?= $category_details->name; ?></h3>
        </div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 cat_box_wrap">
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $myPosts = query_posts(array('posts_per_page' => 20, 'post_type' => 'post', 'paged' => $paged, 'cat' => $category_details->term_id, 'orderby' => 'post_date', 'order' => 'DESC') );
                if (count($myPosts) == 0) {
                    ?>
                    <p>No records found.</p>
                    <?php
                }
                foreach ($myPosts as $post) {
                    $url = get_the_permalink($post->ID);
                    $postDate = get_the_date('F j, Y', $post->ID)
                    ?>
                    <div class="cat_box clearfix">
                        <div class="cat_img">
                            <a href="<?= $url; ?>"><img src="<?= get_post_image_url_with_default($post->ID, 'thumbnail', true); ?>" alt="" /></a>
                        </div>
                        <div class="cat_det">
                            <h4 class="cat_head"><a href="<?= $url; ?>"><?= $post->post_title; ?></a></h4>
                            <div class="tag_line"><span><i class="fa fa-calendar"></i></span> <?= $postDate; ?></div>
                            <div class="cat_limited"><?= limit_words($post->post_content, 20); ?>...</div>
                            <div class="cat_read_more">
                                <a href="<?= $url; ?>">Read More</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <nav class="pagination">
                    <?php pagination_bar(); ?>
                </nav>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">

            </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>