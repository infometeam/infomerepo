<?php

/**
 * Slightly Modified Options Framework
 */
require_once ('admin/index.php');

function getUserIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) //if from shared
    {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //if from a proxy
    {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}

// Word limiter. Don't touch it. Too Risky :P

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	return implode(" ",array_splice($words,0,$word_limit));
}

// Add Menu support. Only Wordpress know what is this

//add_theme_support( 'menus' );
register_nav_menu( 'mainmenu', 'Main Menu' );
register_nav_menu( 'footermenu', 'Footer Menu' );


function bd_setup () {
    add_theme_support('post-thumbnails');
    update_option('thumbnail_size_w', 260);
    update_option('thumbnail_size_H', 228);
    update_option('medium_size_w', 400);
    update_option('medium_size_H', 280);
    /*	update_option('large_size_w', 630);
    update_option('large_size_H', 350);*/
    // Standard Size Thumbnail
    if(false === get_option("thumbnail_crop")) {
        add_option("thumbnail_crop", "1");
    } else {
        update_option("thumbnail_crop", "1");
    }

    // Medium Size Thumbnail
    if(false === get_option("medium_crop")) {
        add_option("medium_crop", "1");
    } else {
        update_option("medium_crop", "1");
    }
}
add_action('init', 'bd_setup');
function rh_show_menu($data)
{
	if($data['type'] == 'basic')
	{
		$registred_locations = get_registered_nav_menus();
		$items = wp_get_nav_menu_items($registred_locations[$data['slug']]);
		$current_page_id = get_the_id();

		foreach($items as $item)
			{


				$active_css_class = NULL;
				if($current_page_id == $item->object_id)
				{$active_css_class = 'active';}
				$html = $html."<li><a href=".$item->url.">".$item->title."</a></li>";
			}
		echo $html;



	}
	else
	{
	$defaults = array(
						'theme_location'  => $data['slug'],
						'menu'            => '',
						'container'       => false,
						'container_class' => $data['class'],
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul class="'.$data['class'].'">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
	wp_nav_menu( $defaults );
	}
}

// Get URL of first image in a post. It is a particular Kariyila. Don't tuch it.

function catch_that_image($contnent)
{
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $contnent, $matches);
	$first_img = $matches [1] [0];
	// No Image found. Total fail :-D
	if(empty($first_img))
	{
		$first_img = get_template_directory_uri()."/images/default.jpg";
	}
return $first_img;
}

function get_post_featured_image_url($id)
{
	$featured_image = get_the_post_thumbnail($id, 'full');
	$image_url = catch_that_image($featured_image);
	return $image_url;
}
function get_feat_test_image_url($id)
{
	$feat_test_image = get_the_post_thumbnail($id, 'normal-thumb');
	$feat_test_image_url = catch_that_image($feat_test_image);
	return $feat_test_image_url;
}
function get_feat_thumb_img($id)
{
	$feat_thumb_img = get_the_post_thumbnail($id, 'thumbnail');
	$feat_thumb_img_url = catch_that_image($feat_thumb_img);
	return $feat_thumb_img_url;
}

// Remove Images Just text

function remove_image_filter($content_text){
$filtered_text = preg_replace("/<img[^>]+\>/i", "", $content_text);
return $filtered_text;
}
// Widgets
if ( function_exists('register_sidebar') )
register_sidebar(array('name' => 'Weather','before_widget' => '','after_widget' => '','before_title' => '<h1>','after_title' => '</h1>'));

if ( function_exists('register_sidebar') )
register_sidebar(array('name' => 'Home Right','before_widget' => '','after_widget' => '','before_title' => '<h1>','after_title' => '</h1>'));

//Return Page type
function current_page_type($page_ID){
$result = get_post_meta($page_ID, 'pagetype', true);
return $result;
}

//Get Category id with name
function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
	return $term->term_id;
}



function get_news_and_updates(){
	$post_array = get_posts(  array(
		'numberposts'		=>	1000,
		'offset'			=>	0,
		'category'			=>	3,
		'orderby'			=>	'post_date',
		'order'				=>	'DESC',
		'include'			=>	'',
		'exclude'			=>	'',
		'meta_key'			=>	'',
		'meta_value'		=>	'',
		'post_type'			=>	'post',
		'post_mime_type'	=>	'',
		'post_parent'		=>	'',
		'post_status'		=>	'publish' )
	);
	foreach ($post_array as $key => $post) {
		# code...

		$post_date = get_post_meta( $post->ID, $key = 'news_date', $single = true );
		$post_url = get_permalink($post->ID);
		$final_out = $final_out.'<li class="clearfix">  <a href="'.$post_url.'" class="rh_more_btn_sm">more</a><span class="dt">'.$post_date.'</span>
            <p><strong>'.$post->post_title.'</strong>'.limit_words($post->post_content,22).'...</p>
            </li>';
	}
	return $final_out;
}
function get_template_option_values($input)
{
	global $data;


	return $data[$input];
}

function get_home_page_text()
{
	$about_id = 2;
	$page_data = get_page($about_id);
	$page_text = $page_data->post_content;
	$page_url = get_permalink($about_id);
	$html = '<h1><span>Welcome to Mafco</span></h1>
    <p>'.$page_text.'</p><a href="'.$page_url.'" class="rh_more_btn">more</a>';
    return $html;
}

function get_product_names_helper($catid)
{
	$post_array = get_posts(  array(
		'numberposts'		=>	1000,
		'offset'			=>	0,
		'category'			=>	$catid,
		'orderby'			=>	'post_date',
		'order'				=>	'DESC',
		'include'			=>	'',
		'exclude'			=>	'',
		'meta_key'			=>	'',
		'meta_value'		=>	'',
		'post_type'			=>	'post',
		'post_mime_type'	=>	'',
		'post_parent'		=>	'',
		'post_status'		=>	'publish' )
	);
	foreach($post_array as $post)
	{
		$url = get_permalink($post->ID);
		$html = $html.'<li><a href="'.$url.'">'.$post->post_title.'</a></li>';
	}
	return '<ul class="rh_prdct_submenu">'.$html.'</ul>';
}

function get_products_menu()
{
	$args = array(
					'type'                     => 'post',
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '1,3',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'category',
					'pad_counts'               => false

				);
	$categories = get_categories($args);
	foreach ($categories as $cats) {
		# code...
		$html = $html.'<li><a href="#">'.$cats->name.'</a>'.get_product_names_helper($cats->term_id).'</li>';
	}
	return '<ul class="rh_prdct_menu">'.$html.'</ul>';
}

function show_products_lists($category_id = NULL)
{
	/*$post_array = get_posts(  array(
		'numberposts'		=>	1000,
		'offset'			=>	0,
		'category'			=>	$category_id,
		'orderby'			=>	'post_date',
		'order'				=>	'DESC',
		'include'			=>	'',
		'exclude'			=>	'1',
		'meta_key'			=>	'',
		'meta_value'		=>	'',
		'post_type'			=>	'post',
		'post_mime_type'	=>	'',
		'post_parent'		=>	'',
		'post_status'		=>	'publish' )
	);*/
	$post_array = query_posts('cat=-3');
	foreach($post_array as $post)
	{
		$url = get_permalink($post->ID);
		$text = limit_words(strip_tags($post->post_content),12);
		$image_url = get_post_featured_image_url($post->ID);
		$html = $html.'<li>
                	<a href="'.$url.'"><img src="'.$image_url.'">
                    <div class="rh_prdct_dtls"><h5>'.$post->post_title.'</h5>
                    <p>'.$text.'...</p></div>
                    </a>
                </li>';
	}
	return $html;
}

function show_client_logo()
{
	global $wpdb;
	$tablename_images = $wpdb->prefix."ngg_pictures";
	$tablename_gallery = $wpdb->prefix."ngg_gallery";
	$query = "SELECT * FROM $tablename_images WHERE galleryid = 1";
	$query_result = $wpdb->get_results($query, ARRAY_A);
	foreach($query_result as $images)
	{
		$gallery_id = $images['galleryid'];
		$get_directory_query = 	"SELECT path FROM $tablename_gallery WHERE gid = '$gallery_id'";
		$directory_name = $wpdb->get_var($get_directory_query);
		$image_url = get_bloginfo('url')."/".$directory_name."/".$images['filename'];
		$html = $html."<li><img src='".$image_url."' width='195' height='110'></li>";

	}
	return $html;
}
function show_main_banner()
{
	$data_array = get_template_option_values('pingu_slider');
	foreach ($data_array as $slide) {
		# code...
		$html = $html.'<div style=\'background-image: url("'.$slide['url'].'");\'></div>';
	}
	return $html;


}



function homeBanner()
{
	$data_array = get_template_option_values('pingu_slider');
	if (count($data_array) > 1) {
	    echo '<div class="owl-carousel owl-theme" id="owl-demo1">';
        foreach ($data_array as $slide) {
            echo '<div class="owl-item"><img src="'.$slide['url'].'" alt=""></div>';
        }
        echo '</div>';
    } else {
        foreach ($data_array as $slide) {
            echo '<img class="img-responsive" src="'.$slide['url'].'" alt="" />';
        }
    }
}




/*
###########
###########
###########
###########
###########
###########
###########
###########
###########
###########
*/
function get_post_image_url_with_default($postId, $imageSize = "thumbnail", $showDefault = true) {
    $imageUrl = null;
    if ($showDefault == true) {
        $imageUrl = get_template_directory_uri()."/images/default_img.jpg";
    }
    if (get_the_post_thumbnail_url($postId, $imageSize)) {
        $imageUrl = get_the_post_thumbnail_url($postId, $imageSize);
    }
    return $imageUrl;
}
function pagination_bar() {
    $web_path = get_template_directory_uri();
    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'mid_size' => 2,
            'prev_text' => '<img src="'.$web_path.'/images/pager_prev.png">',
            'next_text' => '<img src="'.$web_path.'/images/pager_next.png">'
            /*'next_text' => '<i class="fa fa-angle-double-right"></i>',*/
        ));
    }
}

// Nested Nav Object for Specific Parent
function get_nested_sub_pages_of ($parentId = 8) {
    $pages = get_pages(array(
        "parent" => $parentId,
        "hierarchical" => 1
    ));
    $i = 0;
    foreach ($pages as $page) {
        $subPages = get_pages(array(
            "parent" => $page -> ID,
            "hierarchical" => 1
        ));
        $pages[$i] -> sub_pages = $subPages;
        $i += 1;
    }
    return $pages;
}

function get_sub_pages_of ($parentId = 8) {
    $pages = get_pages(array(
        "parent" => $parentId,
        "sort_column" => "menu_order"
    ));
    return $pages;
}

// Functions for Infome

// Product Post Type
function product_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Product', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Products', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Product', 'twentythirteen' ),
        'all_items'           => __( 'All Products', 'twentythirteen' ),
        'view_item'           => __( 'View Product', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Product', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Product', 'twentythirteen' ),
        'update_item'         => __( 'Update Product', 'twentythirteen' ),
        'search_items'        => __( 'Search Product', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'product', 'twentythirteen' ),
        'description'         => __( 'Product Details', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres', 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-cart',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'product', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'product_post_type', 0 );// Product Post Type

// Solution Post Type
function solution_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Solution', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Solution', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Solutions', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Solution', 'twentythirteen' ),
        'all_items'           => __( 'All Solutions', 'twentythirteen' ),
        'view_item'           => __( 'View Solution', 'twentythirteen' ),
        'add_new_item'        => __( 'Add New Solution', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Solution', 'twentythirteen' ),
        'update_item'         => __( 'Update Solution', 'twentythirteen' ),
        'search_items'        => __( 'Search Solution', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'solution', 'twentythirteen' ),
        'description'         => __( 'Solution Details', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres', 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-admin-network',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'solution', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'solution_post_type', 0 );