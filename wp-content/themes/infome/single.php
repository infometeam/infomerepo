<?php
get_header();
$current_post_id = get_the_ID();
$current_post = get_page($current_post_id);
$featured_image = get_post_image_url_with_default($current_post_id, "full", true);
$postDate = get_the_date('F j, Y', $current_post_id);
?>
    <div class="blog_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <h2 class="blog_single_head"><?= $current_post -> post_title; ?></h2>
                    <div class="blog_single_tag_line"><i class="fa fa-calendar"></i> <?= $postDate; ?></div>
                    <div class="blog_single_image"><img class="img-responsive" src="<?= $featured_image; ?>" alt="<?= $current_post -> post_title; ?>"></div>
                    <div class="blog_single_content">
                        <?= $current_post -> post_content; ?>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="featured_list">
                        <h3 class="featured_post_head">Featured Articles</h3>
                        <?php
                        $featuredPosts = get_posts(array(
                            "numberposts" => 5,
                            "cat" => 65
                        ));
                        foreach ($featuredPosts as $featuredPost) {
                            $featuredPostUrl = get_permalink($featuredPost -> ID);
                            $featuredPostImage_url = get_post_image_url_with_default($featuredPost -> ID, "thumbnail", true);
                            $featuredPostPostDate = get_the_date('F j, Y', $featuredPost->ID);
                            ?>
                            <a href="<?= $featuredPostUrl; ?>" class="featured_post">
                                <div class="featured_post_img"><img class="img-responsive" src="<?= $featuredPostImage_url; ?>" alt="<?= $featuredPost -> post_title; ?>"></div>
                                <div class="featured_post_det">
                                    <h4><?= $featuredPost -> post_title; ?></h4>
                                    <p><?= $featuredPostPostDate; ?></p>
                                </div>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom_row"></div>
<?php get_footer(); ?>