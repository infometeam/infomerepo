<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

// Start Home Settings Tab

$of_options[] = array( 	"name" 		=> "Home Settings",
						"type" 		=> "heading"
				);
					
$of_options[] = array( 	"name" 		=> "Hello there!",
						"desc" 		=> "",
						"id" 		=> "introduction",
						"std" 		=> "<h3 style=\"margin: 0 0 10px;\">Welcome to Bodhi Info WP Template control panel</h3>
						Here you can control, tweak, modify your entire site template according to your needs. Developed and maintained by <a href=\"http://bodhiinfo.com\" target=\"_blank\">Bodhi Info Solutions Pvt Ltd</a>",
						"icon" 		=> true,
						"type" 		=> "info"
				);
$of_options[] = array( 	"name" 		=> "Set Your Logo",
						"desc" 		=> "You can change your logo from here at any time.! A png file with 321x100 will do the job.",
						"id" 		=> "media_upload_356",
						// Use the shortcodes [site_url] or [site_url_secure] for setting default URLs
						"std" 		=> "",
						"mod"		=> "min",
						"type" 		=> "media"
				);
$of_options[] = array( 	"name" 		=> "Set Your Favicon",
						"desc" 		=> "You can change your favicon icon.",
						"id" 		=> "media_upload_favicon",
						// Use the shortcodes [site_url] or [site_url_secure] for setting default URLs
						"std" 		=> "",
						
						"type" 		=> "media"
				);
				
$of_options[] = array( 	"name" 		=> "Manage Your Website Sliders",
						"desc" 		=> "Unlimited slider with drag and drop sortings.",
						"id" 		=> "pingu_slider",
						"std" 		=> "",
						"type" 		=> "slider"
				);
$of_options[] = array( 	"name" 		=> "Youtube Video ID",
						"desc" 		=> "Youtube video ID on Homepage",
						"id" 		=> "youtube_video_id",
						"std" 		=> "",
						"type" 		=> "text"
				);

// End of Home Settings Tab
// Start of General Settings Tab

$of_options[] = array( 	"name" 		=> "General Settings",
						"type" 		=> "heading"
				);
$of_options[] = array( 	"name" 		=> "Your Contacts/Social Details",
						"desc" 		=> "Your facebook ID",
						"id" 		=> "facebook_id",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your twitter ID",
						"id" 		=> "twitter_id",
						"std" 		=> "",
						"type" 		=> "text"
				);
				/*$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your LinkedIn URL",
						"id" 		=> "linked_in_url",
						"std" 		=> "",
						"type" 		=> "text"
				);*/
				/*$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Listile URL",
						"id" 		=> "rh_url",
						"std" 		=> "",
						"type" 		=> "text"
				);*/
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your youtube ID",
						"id" 		=> "youtube_id",
						"std" 		=> "",
						"type" 		=> "text"
				);
				/*$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your google+ ID",
						"id" 		=> "googleplus_id",
						"std" 		=> "",
						"type" 		=> "text"
				);*/
				/*$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your instagram ID",
						"id" 		=> "insta_id",
						"std" 		=> "",
						"type" 		=> "text"
				);*/
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your email address",
						"id" 		=> "cc_email_address",
						"std" 		=> "",
						"type" 		=> "text"
				);
				/*$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your phone number(s) (Seperated by coma)",
						"id" 		=> "cc_contact_numbers",
						"std" 		=> "",
						"type" 		=> "text"
				);*/
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Trade enquiry number",
						"id" 		=> "cc_mobile_number_trade",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Customer care number",
						"id" 		=> "cc_mobile_number_care",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your mobile number in UAE",
						"id" 		=> "cc_mobile_number_uae",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your mobile number in India",
						"id" 		=> "cc_mobile_number_india",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Your mobile number in Qatar",
						"id" 		=> "cc_mobile_number_qatar",
						"std" 		=> "",
						"type" 		=> "text"
				);
				$of_options[] = array( 	"name" 		=> "Address (Perinthalmanna)",
						"desc" 		=> "Your address in India (Perinthalmanna)",
						"id" 		=> "address_india_1",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
                $of_options[] = array( 	"name" 		=> "Address (Cochin)",
						"desc" 		=> "Your address in India (Cochin)",
						"id" 		=> "address_india_2",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
                $of_options[] = array( 	"name" 		=> "Address (UAE)",
						"desc" 		=> "Your address in UAE",
						"id" 		=> "address_uae",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
				$of_options[] = array( 	"name" 		=> "Address (KSA)",
						"desc" 		=> "Your address in KSA",
						"id" 		=> "address_ksa",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
				

$of_options[] = array( 	"name" 		=> "Tracking Code",
						"desc" 		=> "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template.",
						"id" 		=> "google_analytics",
						"std" 		=> "",
						"type" 		=> "textarea"
				);

// Backup Options
$of_options[] = array( 	"name" 		=> "Backup Options",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider.png"
				);
				
$of_options[] = array( 	"name" 		=> "Backup and Restore Options",
						"id" 		=> "of_backup",
						"std" 		=> "",
						"type" 		=> "backup",
						"desc" 		=> 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
				);
				
$of_options[] = array( 	"name" 		=> "Transfer Theme Options Data",
						"id" 		=> "of_transfer",
						"std" 		=> "",
						"type" 		=> "transfer",
						"desc" 		=> 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".',
				);
				
	}//End function: of_options()


}//End chack if function exists: of_options()
?>
