<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Infome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Signika:400,600" rel="stylesheet">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/pratical.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/menu.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/ddmenu.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/css/style.css">
    <link rel="stylesheet" href="<?= bloginfo("stylesheet_url"); ?>">
    <?php wp_head(); ?>
</head>
<body>
<header>
    <!-- <header id="to-affix" class="navbar-inverse" data-spy="affix" data-offset-top="0">-->
    <!--<div class="nav_bar navbar navbar-inverse" id="to-affix">-->
    <div class="container">
        <div class="nav_bar">
            <div class="logo">
                <a href="<?= site_url(); ?>"><img src="<?= get_template_directory_uri(); ?>/images/logo.png" alt="Lameon Malappuaram"></a>
            </div>
            <div class="top_contact"><a href="tel:+97143546020"><span><i class="fa fa-phone" aria-hidden="true"></i>+97143546020</span></a>
                <a href="mailto:sales@infomeuae.com"><span><i class="fa fa-envelope-o" aria-hidden="true"></i>sales@infomeuae.com</span></a>
            </div>
            <nav class="main_nav" id="ddmenu">
                <ul>
                    <li><a href="<?= site_url(); ?>">Home</a></li>
                    <li><a href="#">What we do</a></li>
                    <li><a href="#">Who we are</a></li>
                    <li class="full-width"><a href="#">Products
                            <i class="caret"></i></a>
                        <div class="dropdown">
                            <div class="dd-inner">
                                <div class="row">
                                    <ul class="column col-lg-3">
                                        <li><a href="servers.php"><h3>Server &amp; Storage</h3></a></li>
                                        <li><a href="hp_server.php">HP server</a></li>
                                        <li><a href="dell_server">Dell server</a></li>
                                        <li><a href="#">IBM server</a></li>
                                        <li><a href="#">STORAGE</a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="#"><h3>Desktops</h3></a></li>
                                        <li><a href="#">HP</a></li>
                                        <li><a href="#">Dell</a></li>
                                        <li><a href="#">Lenovo</a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="#"><h3>Laptops</h3></a></li>
                                        <li><a href="#">HP</a></li>
                                        <li><a href="#">Dell</a></li>
                                        <li><a href="#">Toshiba</a></li>
                                        <li><a href="#">Lenovo</a></li>
                                        <li><a href="#">Acer</a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="#"><h3>Printers and Scanners</h3></a></li>
                                        <li><a href="#">HP</a></li>
                                        <li><a href="#">Canon</a></li>
                                        <li><a href="#">Kyocera</a></li>
                                        <li><a href="#">Xerox</a></li>
                                    </ul>
                                    <ul class="column col-lg-3">
                                        <li><a href="#"><h3>Network Products</h3></a></li>
                                        <li><a href="#">Switches</a></li>
                                        <li><a href="#">Routers</a></li>
                                        <li><a href="#">Access Points</a></li>
                                        <li><a href="#">Cabinets</a></li>
                                        <li><a href="#">UTP and Fiber Optic Cables</a></li>
                                        <li><a href="#">Network Accessories</a></li>
                                    </ul>
                                </div>
                                <hr>
                                <div class="row">
                                    <ul class="column col-lg-4">
                                        <li><a href="#"><h3>Software Products</h3></a></li>
                                        <li><a href="#">MICROSOFT SOFTWARES</a></li>
                                        <li><a href="#">Tally</a></li>
                                        <li><a href="#">Anti Virus</a></li>
                                    </ul>
                                    <ul class="column col-lg-4">
                                        <li><a href="#"><h3>Point of Sale</h3></a></li>
                                        <li><a href="#">BARCODE LABEL PRINTERS</a></li>
                                        <li><a href="#">Bar code scanners</a></li>
                                        <li><a href="#">Receipt printers</a></li>
                                        <li><a href="#">PDT</a></li>
                                        <li><a href="#">Cash drawers, POLE DISPLAY MSR READER, PROGRAMMABLE POS
                                                KEYBOARD</a></li>
                                        <li><a href="#">ANTITHEFT , PEOPLE COUNTING,</a></li>
                                    </ul>
                                    <ul class="column col-lg-4">
                                        <li><a href="#"><h3>ID card printers and consumables</h3></a></li>
                                        <li><a href="#">fargo ID card pritners</a></li>
                                        <li><a href="#">nisca ID card printers</a></li>
                                        <li><a href="#">data card id card printers</a></li>
                                        <li><a href="#"><h3>Accessories</h3></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="#">Solutions</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </nav>
            <div class="open mobile">
                <span class="cls"></span>
                <span>
                    <ul class="sub-menu ">
                        <li>
                            <a href="#about" title="about">Home</a>
                        </li>
                        <li>
                            <a href="#skills" title="skills">What we do</a>
                        </li>
                        <li>
                            <a href="#jobs" title="jobs">Solutions</a>
                        </li>
                        <li>
                            <a href="#contact" title="contact">Products</a>
                        </li>
                        <li>
                            <a href="#contact" title="contact">Contact us</a>
                        </li>
                    </ul>
                </span>
                <span class="cls"></span>
            </div>
        </div>
    </div>

</header>