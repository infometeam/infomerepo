<?php
get_header();
$current_post_id = get_the_ID();
$current_post = get_page($current_post_id);
$featured_image = get_post_image_url_with_default($current_post_id, 'full', true);
$postUrl = get_permalink();
?>
    <div class="inner inner_secnd" id="main">
        <div class="product_details">
            <div class="container">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <h1><?= $current_post -> post_title; ?></h1>
                    <img class="img-responsive" src="<?= $featured_image; ?>" alt="<?= $current_post -> post_title; ?>">
                    <div class="solution_dets">
                        <?= $current_post -> post_content; ?>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div class="solution_form_wrapper">
                        <h4 class="solution_form_head">Enquire Now</h4>
                        <div class="solution_form">
                            <?= do_shortcode('[contact-form-7 id="237" title="Solution Enquiry"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom_row"></div>
<?php get_footer(); ?>